/**
 * Clone the course contents (except for the student-generated contents).
 *
 * We use the "origin" field to store the ID of the document we cloned a document from.
 * Note that the origin document is in the same collection,
 * but in a different course (and thus not published with the cloned course).
 *
 * Note: We need to clone the tables in the right order,
 *       so that we populate the xxxIdMap before we use it.
 */
Meteor.methods({
	cloneCourse: function(courseId) {
		console.log("Meteor.methods.cloneCourse("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to clone a course.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to clone a course.");
		}
		var userId = this.userId;
		var now = new Date();

		// Courses
		console.log("Cloning Course:");
		console.log("courseId:", courseId);
		var instance = course.instance;
		var newInstance;
		if (Number(instance)===instance && instance%1===0) {
			newInstance = instance+1;
		} else {
			newInstance = instance+"+";
		}
		var newCourseId = Courses.insert(
			{
				origin: courseId,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				abbreviation: course.abbreviation,
				instance: newInstance,
				title: course.title,
				visible: false,
				openForRegistration: false,
				ended: false,
				startDate: now,
				endDate: now,
				masteryChecksDetermineGrade: course.masteryChecksDetermineGrade,
				maxTopicsPerMasteryCheck: course.maxTopicsPerMasteryCheck,
				useQuizzes: course.useQuizzes,
				syllabus: course.syllabus,
				bookListIntroduction: course.bookListIntroduction,
				labListIntroduction: course.labListIntroduction,
				classSessionListIntroduction: course.classSessionListIntroduction,
				description: course.description,
				logoUrl: course.logoUrl,
				overview: course.overview,
				welcomeVideoUrl: course.welcomeVideoUrl,
			}
		);
		console.log("new courseId:", newCourseId);

		// Themes
		console.log("Cloning Themes:");
		var themeIdMap = {};
		var themes = Themes.find({courseId: courseId, removed: {$ne: true}});
		themes.forEach(function (theme) {
			var newThemeId = Themes.insert({
				origin: theme._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				title: theme.title,
				description: theme.description,
				icon: theme.icon,
				color: theme.color,
				why: theme.why,
			});
			//console.log("theme._id:", theme._id);
			//console.log("newThemeId:", newThemeId);
			themeIdMap[theme._id] = newThemeId;
			//console.log("themeIdMap[theme._id]:", themeIdMap[theme._id]);
		});

		// Topics
		console.log("Cloning Topics:");
		var topicIdMap = {};
		var topics = Topics.find({courseId: courseId, removed: {$ne: true}});
		topics.forEach(function(topic){
			var newTopicId = Topics.insert({
				origin: topic._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				themeId: themeIdMap[topic.themeId],
				title: topic.title,
				description: topic.description,
				why: topic.why,
				studyGuide: topic.studyGuide,
				masteryRequiredForGrade: topic.masteryRequiredForGrade,
			});
			topicIdMap[topic._id] = newTopicId;
		});

		// Books
		console.log("Cloning Books:");
		var bookIdMap = {};
		var books = Books.find({courseId: courseId, removed: {$ne: true}});
		books.forEach(function(book){
			var newBookId = Books.insert({
				origin: book._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				abbreviation: book.abbreviation,
				title: book.title,
				subtitle: book.subtitle,
				author: book.author,
				url: book.url,
				imageUrl: book.imageUrl,
				description: book.description,
			});
			bookIdMap[book._id] = newBookId;
		});

		// Chapters
		console.log("Cloning Chapters:");
		var chapterIdMap = {};
		var chapters = Chapters.find({courseId: courseId, removed: {$ne: true}});
		chapters.forEach(function(chapter){
			var newChapterId = Chapters.insert({
				origin: chapter._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				bookId: bookIdMap[chapter.bookId],
				title: chapter.title,
				number: chapter.number,
				sortKey: chapter.sortKey,
			});
			chapterIdMap[chapter._id] = newChapterId;
		});

		// Sections
		console.log("Cloning Sections:");
		var sectionIdMap = {};
		var sections = Sections.find({courseId: courseId, removed: {$ne: true}});
		sections.forEach(function(section){
			var newSectionId = Sections.insert({
				origin: section._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				bookId: bookIdMap[section.bookId],
				chapterId: chapterIdMap[section.chapterId],
				title: section.title,
				number: section.number,
				sortKey: section.sortKey,
			});
			sectionIdMap[section._id] = newSectionId;
		});

		// Skills
		console.log("Cloning Skills:");
		var skillIdMap = {};
		var skills = Skills.find({courseId: courseId, removed: {$ne: true}});
		skills.forEach(function(skill){
			var newSkillId = Skills.insert({
				origin: skill._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				topicId: topicIdMap[skill.topicId],
				title: skill.title,
				description: skill.description,
			});
			skillIdMap[skill._id] = newSkillId;
		});

		// StudyTasks
		console.log("Cloning StudyTasks:");
		var studyTaskIdMap = {};
		var studyTasks = StudyTasks.find({courseId: courseId, removed: {$ne: true}});
		studyTasks.forEach(function(studyTask){
			var data = {
				origin: studyTask._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				topicId: topicIdMap[studyTask.topicId],
				title: studyTask.title,
				description: studyTask.description,
				kind: studyTask.kind,
				body: studyTask.body,
				detail: studyTask.detail,
			};
			if (data.kind==="Textbook" && data.detail) {
				data.detail.bookId = bookIdMap[data.detail.bookId];
				data.detail.chapterId = chapterIdMap[data.detail.chapterId]; // was buggy (bookIdMap[data.detail.chapterId])
				data.detail.sectionIds = _.map(data.detail.sectionIds, function(sectionId) {return sectionIdMap[sectionId];});
			}
			if (data.kind==="ClassSession" && data.detail) {
				// map classSessionId -- circular dependency, defer to after cloning ClassSessions
				//data.detail.classSessionId = classSessionIdMap[data.detail.classSessionId];
			}
			var newStudyTaskId = StudyTasks.insert(data);
			studyTaskIdMap[studyTask._id] = newStudyTaskId;
		});

		// ClassSessions
		console.log("Cloning ClassSessions:");
		var classSessionIdMap = {};
		var classSessions = ClassSessions.find({courseId: courseId, removed: {$ne: true}});
		classSessions.forEach(function(classSession){
			var data = {
				origin: classSession._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				creationDate: now,
				date: classSession.date, // will be in the past
				status: "hidden", // make them all hidden (instructor will show them once scheduled)
				title: classSession.title,
				description: classSession.description,
				duration: classSession.duration,
				preparation: classSession.preparation,
				materials: classSession.materials,
			};
			if (classSession.topicIds) {
				data.topicIds = _.map(classSession.topicIds, function(topicId) {return topicIdMap[topicId];});
			}
			if (classSession.studyTaskIds) {
				data.studyTaskIds = _.map(classSession.studyTaskIds, function(studyTaskId) {return studyTaskIdMap[studyTaskId];});
			}
			var newClassSessionId = ClassSessions.insert(data);
			classSessionIdMap[classSession._id] = newClassSessionId;
		});
		// fix id mapping for circular dependency StudyTask <-> ClassSession
		StudyTasks.find({courseId: newCourseId, kind: "ClassSession"}).forEach(function(studyTask) {
			if (studyTask.detail && studyTask.detail.classSessionId) {
				StudyTasks.update({_id: studyTask._id}, {
					$set: {
						"detail.classSessionId": classSessionIdMap[studyTask.detail.classSessionId]
					}
				});
			}
		});

		// Labs
		console.log("Cloning Labs:");
		var labIdMap = {};
		var labs = Labs.find({courseId: courseId, removed: {$ne: true}});
		labs.forEach(function(lab){
			var topicIds = lab.topicIds;
			var newTopicIds = _.map(topicIds, function(topicId) {return topicIdMap[topicId];});
			var newLabId = Labs.insert({
				origin: lab._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				title: lab.title,
				description: lab.description,
				topicIds: newTopicIds,
				status: "hidden",
				deadlineDate: lab.deadlineDate, // will be in the past
				groupSize: lab.groupSize,
			});
			//console.log("lab._id:", lab._id);
			//console.log("newLabId:", newLabId);
			labIdMap[lab._id] = newLabId;
			//console.log("labIdMap[lab._id]:", labIdMap[lab._id]);
		});

		// ContentItems
		console.log("Cloning ContentItems:");
		var contentItemIdMap = {};
		var contentItems = ContentItems.find({courseId: courseId, removed: {$ne: true}});
		contentItems.forEach(function(contentItem){
			//console.log("original item:", contentItem);
			//console.log("contentItem._id:", contentItem._id);
			//console.log("contentItem.containerId:", contentItem.containerId);
			var newContainerId;
			if (contentItem.containerCollection==="Labs") {
				newContainerId = labIdMap[contentItem.containerId];
			} else {
				console.log("WARNING: Found content item for unsupported containerCollection '"+contentItem.containerCollection+"'");
				console.log("original item:", contentItem);
			}
			var data = {
				origin: contentItem._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				containerCollection: contentItem.containerCollection,
				containerId: newContainerId,
				kind: contentItem.kind,
				index: contentItem.index,
				isNewlyCreated: contentItem.isNewlyCreated,
			};
			switch (contentItem.kind) {
				case "section":
				case "subsection":
				case "subsubsection":
					data.title = contentItem.title;
					break;
				case "markdown":
					data.text = contentItem.text;
					break;
				case "code":
					data.lang = contentItem.lang;
					data.text = contentItem.text;
					break;
				case "question":
					data.type = contentItem.type;
					data.text = contentItem.text;
					data.contents = contentItem.contents;
					break;
				case "image":
					data.url = contentItem.url;
					break;
				default:
					console.log("WARNING: Found content item of unsupported kind '"+contentItem.kind+"'");
					console.log("original item:", contentItem);
			}
			//console.log("inserting:", data);
			var newContentItemId = ContentItems.insert(data);
			contentItemIdMap[contentItem._id] = newContentItemId;
		});

		// Quizzes
		console.log("Cloning Quizzes:");
		var quizPracticeProblemIds = [];
		var quizIdMap = {};
		var quizzes = Quizzes.find({courseId: courseId, removed: {$ne: true}});
		quizzes.forEach(function(quiz){
			var newTopicIds = _.map(quiz.topicIds, function(topicId) {return topicIdMap[topicId];});
			quizPracticeProblemIds= _.union(quizPracticeProblemIds, quiz.practiceProblemIds);
			var newQuizId = Quizzes.insert({
				origin: quiz._id,
				log: [{what: "Cloned", when: now, who: userId}],
				removed: false,
				courseId: newCourseId,
				title: quiz.title,
				description: quiz.description,
				date: quiz.date, // will be in the past
				duration: quiz.duration,
				topicIds: newTopicIds,
				practiceProblemIds: quiz.newPracticeProblemIds, //note: we will resolve circular id reference (after cloning PPs)
				status: "hidden",
			});
			quizIdMap[quiz._id] = newQuizId;
		});




		//**** Clone a useful subset of user-generated content ****

		// Q: should we clone any registrations (e.g., the instructors)?
		//      if we do, we won't be able to delete the cloned course
		//      if we don't, who will OWN all the cloned user-generated contents?
		//      - the original creator (who would be able to see it once they register for the cloned course)

		// Annotations
		// Q: only clone Annotations created by the course instructors?
		// A: no, only clone non-question annotations:
		//    this way a repeating student (and the teaching team) will get to see
		//    their old bookmarks, tags, and notes
		console.log("Cloning Annotations:");
		var annotationIdMap = {};
		var annotations = Annotations.find({courseId: courseId, removed: {$ne: true}});
		annotations.forEach(function(annotation){
			//console.log("original annotation:", annotation);
			//console.log("annotation._id:", annotation._id);
			if (annotation.kind!=='question') {
				// we do not clone questions
				// (we want students of this new instance of the course to come up with their own)
				var data = {
					origin: annotation._id,
					log: [{what: "Cloned", when: now, who: userId}],
					courseId: newCourseId,
					userId: annotation.userId,
					creationDate: annotation.creationDate,
					modificationDate: annotation.modificationDate,
					removed: false,
					kind: annotation.kind,
					text: annotation.text,
					// we are not cloning responses and score/votes
					score: 0,
					//upvoteUserIds
					//downvoteUserIds
				}
				if (annotation.skillId) {
					// for skill annotations:
					//console.log("annotation.skillId:", annotation.skillId);
					var newSkillId = skillIdMap[annotation.skillId];
					//console.log("newSkillId:", newSkillId);
					data.skillId = newSkillId;
				} else if (annotation.contentItemId) {
					// for content item annotations:
					//console.log("annotation.contentItemId:", annotation.contentItemId);
					var newContentItemId = contentItemIdMap[annotation.contentItemId];
					//console.log("newContentItemId:", newContentItemId);
					data.contentItemId = newContentItemId;
				} else if (annotation.sectionId) {
					// for section annotations:
					//console.log("annotation.sectionId:", annotation.sectionId);
					var newSectionId = sectionIdMap[annotation.sectionId];
					//console.log("newSectionId:", newSectionId);
					data.sectionId = newSectionId;
				} else {
					console.log("WARNING: Annotation is neither for a Skill nor ContentItem nor Section");
					console.log("original annotation:", annotation);
				}
				//console.log("inserting:", data);
				var newAnnotationId = Annotations.insert(data);
				annotationIdMap[annotation._id] = newAnnotationId;
			}
		});


		// PracticeProblems
		// Note: The person cloning the course owns the cloned PracticeProblems
		console.log("Cloning PracticeProblems:");
		var practiceProblemIdMap = {};
		var practiceProblems = PracticeProblems.find({courseId: courseId, removed: {$ne: true}});
		practiceProblems.forEach(function(practiceProblem){
			//console.log("original practiceProblem:", practiceProblem);
			//console.log("practiceProblem._id:", practiceProblem._id);
			if (_.contains(quizPracticeProblemIds, practiceProblem._id) || (practiceProblem.featured && !practiceProblem.archived)) {
				// we clone PracticeProblems used as quiz questions, and
				// we clone featured unarchived problems
				// (the instructor apparently deemed featured problems worthy)
				var skillIds = practiceProblem.skillIds;
				var newSkillIds = _.map(skillIds, function(skillId) {return skillIdMap[skillId];});

				var data = {
					origin: practiceProblem._id,
					log: [{what: "Cloned", when: now, who: userId}],
					courseId: newCourseId,
					topicId: topicIdMap[practiceProblem.topicId],
					skillIds: newSkillIds,
					userId: userId, // cloner becomes owner
					createdDate: now,
					kind: practiceProblem.kind,
					title: practiceProblem.title,
					question: practiceProblem.question,
					choices: practiceProblem.choices,
					explanation: practiceProblem.explanation,
					featured: false, // we do not feature them; the instructor should decide
					published: false, // we do not publish them; the instructor should decide
					archived: false,
				}
				//console.log("inserting:", data);
				var newPracticeProblemId = PracticeProblems.insert(data);
				practiceProblemIdMap[practiceProblem._id] = newPracticeProblemId;
			}
		});
		// fix id mapping for circular dependency Quizzes <-> PracticeProblems
		Quizzes.find({courseId: newCourseId}).forEach(function(quiz) {
			var newPracticeProblemIds = _.map(quiz.practiceProblemIds, function(practiceProblemId) {return practiceProblemIdMap[practiceProblemId];});
			Quizzes.update({_id: quiz._id}, {
				$set: {
					"practiceProblemIds": newPracticeProblemIds
				}
			});
		});





		//-------- Copy FileCollection contents
		// Problem: references to files via URLs may be embedded all over the place
		//		e.g., in Markup code inside labs, inside topic descriptions, study tasks, ...
		// We could do a text search/replace of URLs in all that kind of contents
		// but that seems a bit risky.
		// HOWEVER, WE ARE LUCKY:
		// As a positive side effect on how we refer to files via URLs,
		// by using the MD5 hash of the file contents,
		// we do NOT need to update the URLs :)
		// We will have two copies of the file, with the same contents, and the same MD5.
		// The old copy might eventually disappear (e.g., if we fully delete the course),
		// but both copies are accessed via the same URL,
		// and we will just get whatever file with that MD5 still exists.
		// So, we're perfectly fine!
		console.log("Cloning CourseFiles:");
		var courseFilesIdMap = {};
		var courseFiles = CourseFiles.find({"metadata.courseId": courseId});
		courseFiles.forEach(function(courseFile){
			console.log("original courseFile:", courseFile);
			console.log("courseFile._id:", courseFile._id);
			var newId = CourseFiles.insert({
				filename: courseFile.filename,
				contentType: courseFile.contentType,
				metadata: {
					courseId: newCourseId,
					ownerId: userId, // cloner becomes owner
					origin: courseFile._id,
					log: [{what: "Cloned", when: now, who: userId}],
				}
			});
			console.log("newId:", newId);
			var readStream = CourseFiles.findOneStream({_id: courseFile._id});
			readStream.on("error", function(error) {
				console.log("error reading from "+courseFile._id);
			});
			var writeStream = CourseFiles.upsertStream({_id: newId});
			writeStream.on("error", function(error) {
				console.log("error writing to "+newId);
			});
			writeStream.on("close", function(error) {
				console.log("success; closed "+newId);
			});
			// copy
			// note: this will happen asynchronously
			readStream.pipe(writeStream);
		});

	}
});
