Meteor.methods({
  // A teaching team member enables each student to take the quiz
  // by creating a quiz attempt.
  // This way students cannot cheat and start a quiz
  // from outside the physical classroom
	createQuizAttempt: function(courseId, quizId, studentId) {
		var now = new Date();
		logMethodCall(now, "createQuizAttempt", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create a quiz attempt.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!quiz.ongoing) {
			throw new Meteor.Error("quiz-not-ongoing", "The specified quiz is not ongoing.");
		}
		if (!isOnTeachingTeam(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to create a quiz attempt for this quiz.");
		}
		QuizAttempts.insert({
			courseId: courseId,
      quizId: quizId,
			quizStartDate: quiz.startDate,
      studentId: studentId,
      creatorId: this.userId,
      creationDate: now,
      fresh: true,
      ongoing: false,
      finished: false,
    });
  },
  // A student begins a quiz attempt (that had been created by a tt member).
  // This starts the time (the quiz determines how much time an attempt can take).
  beginQuizAttempt: function(quizId) {
		var now = new Date();
		logMethodCall(now, "beginQuizAttempt", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create a quiz attempt.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!quiz.ongoing) {
			throw new Meteor.Error("quiz-not-ongoing", "The specified quiz is not ongoing.");
		}
    var quizAttempt = QuizAttempts.findOne({quizId: quizId, studentId: this.userId, fresh: true});
		if (!quizAttempt) {
			throw new Meteor.Error("no-quiz-attempt", "You don't have a fresh quiz attempt for this quiz.");
		}
		QuizAttempts.update({quizId: quizId, studentId: this.userId, fresh: true}, {
      $set: {
        startDate: now,
        fresh: false,
        ongoing: true,
        finished: false,
      },
    });
  },
  // A student finishes their own quiz attempt.
  // If they don't do that, the attempt will automatically finish once it times out,
	// or once a tt member stops the quiz.
  finishQuizAttempt: function(quizId, solutions) {
		var now = new Date();
		logMethodCall(now, "finishQuizAttempt", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create a finish attempt.");
		}
    var quizAttempt = QuizAttempts.findOne({quizId: quizId, studentId: this.userId, ongoing: true});
		if (!quizAttempt) {
			throw new Meteor.Error("no-quiz-attempt", "You don't have an ongoing quiz attempt for this quiz.");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.finishQuizAttempt: quizId:", quizId, "quizAttemptId:", quizAttempt._id, "userId:", this.userId, "date:", now, "solutions:", solutions);
		QuizAttempts.update({_id: quizAttempt._id}, {
      $set: {
        endDate: now,
        fresh: false,
        ongoing: false,
        finished: true,
				solutions: solutions,
      },
    });
  },
});
