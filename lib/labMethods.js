Meteor.methods({
	/* This was a throw-away method, used to convert from the old representation of lab content items.
	convertLabItems: function(labId) {
		console.log("Meteor.methods.convertLabItems("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isInstructor(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this lab.");
		}
		var now = new Date();
		var userId = this.userId;
		_.each(lab.body, function(item, index, array) {
			console.log("Item at index "+index);
			console.log(item);
			// augment item
			item.containerId = labId;          // which thing are we part of
			item.containerCollection = "Labs"; // what kind of thing are we part of?
			item.courseId = lab.courseId;
			item.index = index;
			item.log = [{what: "Created", when: now, who: userId}];
			// insert item
			ContentItems.insert(item);
		});
	},
	*/

	labSubmissionFileUploadFinished: function(labSubmissionFileId) {
		var now = new Date();
		logMethodCall(now, "labSubmissionFileUploadFinished", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var file = LabSubmissionFiles.findOne({_id: new Mongo.ObjectID(labSubmissionFileId)});
		if (!file) {
			throw new Meteor.Error("file-does-not-exist", "The specified file does not exist.");
		}
		var courseId = file.metadata.courseId;
		if (!isRegisteredAsAnything(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit. How could you actually upload this?");
		}
		var labId = file.metadata.labId;
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("no-lab", "There is no lab for this file");
		}
		if (lab.status!="open") {
			throw new Meteor.Error("lab-not-open", "You cannot submit to a lab that's not open. How could you actually upload this?");
		}
		var labItemId = file.metadata.labItemId;
		var labItem = ContentItems.findOne({_id: labItemId});
		if (!labItem) {
			throw new Meteor.Error("lab-item-not-found", "The lab item specified in the file does not exist.");
		}
		var labTeam = LabTeams.findOne({labId: labId, memberIds: this.userId});
		if (!labTeam) {
			throw new Meteor.Error("no-lab-team", "You are in no lab team for the lab this item belongs to");
		}
		if (labTeam._id != file.metadata.labTeamId) {
			throw new Meteor.Error("lab-team-ids-mismatch", "The lab team specified in the file, and your lab team for the lab specified in the file differ");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.labSubmissionFileUploadFinished: labId:", lab._id, "labTeamId:", labTeam._id, "userId:", this.userId, "labTeam.memberIds:", labTeam.memberIds, "date:", now, "itemId:", labItem._id, "file:", file);
		FeedEvents.insert({userId: this.userId, courseId: lab.courseId, date: now, active: true, event: "IUploadedALabSubmissionFile", labSubmissionFileId: labSubmissionFileId});
	},
	saveContentItemSubmission: function(itemId, value) {
		var now = new Date();
		logMethodCall(now, "saveContentItemSubmission", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var item = ContentItems.findOne({_id: itemId});
		if (!item) {
			throw new Meteor.Error("item-does-not-exist", "The specified item does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, item.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit.");
		}
		// Note: this makes ContentItemSubmissions only work with labs!!!
		var lab = Labs.findOne({_id: item.containerId});
		if (!lab) {
			throw new Meteor.Error("no-lab", "There is no lab for this content item");
		}
		if (lab.status!="open") {
			throw new Meteor.Error("lab-not-open", "You cannot submit to a lab that's not open.");
		}
		var labTeam = LabTeams.findOne({labId: item.containerId, memberIds: this.userId});
		if (!labTeam) {
			throw new Meteor.Error("no-lab-team", "You are in no lab team for the lab this item belongs to");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.saveContentItemSubmission: labId:", lab._id, "labTeamId:", labTeam._id, "userId:", this.userId, "labTeam.memberIds:", labTeam.memberIds, "date:", now, "itemId:", item._id, "value:", value);
		ContentItemSubmissions.update({
			itemId: itemId,
			labTeamId: labTeam._id,
		}, {
			$set: {
				courseId: item.courseId,
				itemId: itemId,
				labTeamId: labTeam._id,
				userIds: labTeam.memberIds, // maintain these for pub/sub on userId
				submitterId: this.userId,
				modificationDate: now,
				value: value,
			},
			$push: {
				log: {what: "Saved", when: now, who: this.userId, value: value},
			},
		}, {
			upsert: true,
		});
		var contentItemSubmission = ContentItemSubmissions.findOne({itemId: itemId, labTeamId: labTeam._id});
		FeedEvents.insert({userId: this.userId, courseId: item.courseId, date: now, active: true, event: "ISubmittedAContentItemSubmission", contentItemSubmissionId: contentItemSubmission._id});
		return contentItemSubmission._id;
	},
	saveReferenceContentItemSubmission: function(itemId, value) {
		var now = new Date();
		logMethodCall(now, "saveReferenceContentItemSubmission", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var item = ContentItems.findOne({_id: itemId});
		if (!item) {
			throw new Meteor.Error("item-does-not-exist", "The specified item does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, item.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit a reference submission.");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.saveReferenceContentItemSubmission: labTeamId: null, userId:", this.userId, "date:", now, "itemId:", item._id, "value:", value);
		ContentItemSubmissions.update({
			itemId: itemId,
			labTeamId: null,
		}, {
			$set: {
				courseId: item.courseId,
				itemId: itemId,
				labTeamId: null,
				userIds: null, // maintain these for pub/sub on userId
				submitterId: this.userId,
				modificationDate: now,
				value: value,
			},
			$push: {
				log: {what: "Saved", when: now, who: this.userId, value: value},
			},
		}, {
			upsert: true,
		});
		var contentItemSubmission = ContentItemSubmissions.findOne({itemId: itemId, labTeamId: null});
		FeedEvents.insert({userId: this.userId, courseId: item.courseId, date: now, active: true, event: "ISubmittedAContentItemSubmission", contentItemSubmissionId: contentItemSubmission._id});
		return contentItemSubmission._id;
	},

	// deprecated?
	createContentItemQuestion: function(itemId) {
		console.log("Meteor.methods.createContentItemQuestion("+itemId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var item = ContentItems.findOne({_id: itemId});
		if (!item) {
			throw new Meteor.Error("item-does-not-exist", "The specified item does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, item.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create a question.");
		}
		var now = new Date();
		var userId = this.userId;
		return ContentItemQuestions.insert({
			courseId: item.courseId,
			itemId: itemId,
			userId: userId,
			creationDate: now,
			modificationDate: now,
			removed: false,
			log: [{what: "Created", when: now, who: userId}],
		});
	},
	// deprecated?
	updateContentItemQuestion: function(questionId, text) {
		console.log("Meteor.methods.updateContentItemQuestion("+questionId+", "+text+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var question = ContentItemQuestions.findOne({_id: questionId, removed: false});
		if (!question) {
			throw new Meteor.Error("question-does-not-exist", "The specified question does not exist.");
		}
		if (question.userId!==this.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this question.");
		}
		var now = new Date();
		var userId = this.userId;
		return ContentItemQuestions.update({_id: questionId}, {
			$set: {
				modificationDate: now,
				text: text,
			},
			$push: {
				log: {what: "Modified", when: now, who: userId},
			},
		});
	},
	// deprecated?
	removeContentItemQuestion: function(questionId) {
		console.log("Meteor.methods.removeContentItemQuestion("+questionId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var question = ContentItemQuestions.findOne({_id: questionId, removed: false});
		if (!question) {
			throw new Meteor.Error("question-does-not-exist", "The specified question does not exist.");
		}
		if (question.userId!==this.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this question.");
		}
		var now = new Date();
		var userId = this.userId;
		return ContentItemQuestions.update({_id: questionId}, {
			$set: {
				removalDate: now,
				removed: true,
			},
			$push: {
				log: {what: "Removed", when: now, who: userId},
			},
		});
	},
});
