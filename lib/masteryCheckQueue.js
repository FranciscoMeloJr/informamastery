Meteor.methods({
  enqueueMasteryCheckRegistration: function(masteryCheckRegistrationId) {
    var now = new Date();
    logMethodCall(now, "enqueueMasteryCheckRegistration", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to enqueue a mastery check registration.");
    }
    var masteryCheckRegistration = MasteryCheckRegistrations.findOne({_id: masteryCheckRegistrationId});
    if (!masteryCheckRegistration) {
      throw new Meteor.Error("no-such-mastery-check-registration", "Mastery check registration not found.");
    }
    var masteryCheckSession = MasteryCheckSessions.findOne({courseId: masteryCheckRegistration.courseId, ongoing: true});
    if (!masteryCheckSession) {
      throw new Meteor.Error("no-ongoing-mastery-check-session", "No ongoing mastery check session found.");
    }
    if (!isRegisteredAsAnything(this.userId, masteryCheckRegistration.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-registered", "You must be registered in this course to enqueue a mastery check registration.");
		}
    return MasteryCheckQueueEntries.insert({
      courseId: masteryCheckRegistration.courseId,
      masteryCheckSessionId: masteryCheckSession._id,
      creatorId: this.userId,
			creationDate: now,
      masteryCheckRegistrationId: masteryCheckRegistrationId,
		});
  },
  dequeueMasteryCheckRegistration: function(masteryCheckRegistrationId) {
    var now = new Date();
    logMethodCall(now, "dequeueMasteryCheckRegistration", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to dequeue a mastery check registration.");
    }
    var masteryCheckRegistration = MasteryCheckRegistrations.findOne({_id: masteryCheckRegistrationId});
    if (!masteryCheckRegistration) {
      throw new Meteor.Error("no-such-mastery-check-registration", "Mastery check registration not found.");
    }
    if (!isRegisteredAsAnything(this.userId, masteryCheckRegistration.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-registered", "You must be registered in this course to dequeue a mastery check registration.");
		}
    var result = MasteryCheckQueueEntries.remove({
      courseId: masteryCheckRegistration.courseId,
      creatorId: this.userId,
      masteryCheckRegistrationId: masteryCheckRegistrationId,
		});
    console.log(result);
  },

});
