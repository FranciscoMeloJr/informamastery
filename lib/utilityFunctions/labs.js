// JavaScript functions related to labs, usable on client AND server

getPastLabs = function(courseId) {
  var now = new Date();
  return Labs.find({courseId: courseId, removed: false, deadlineDate: {$lt: now}}, {sort: {deadlineDate: 1}});
};

getPastNonHiddenLabs = function(courseId) {
  var now = new Date();
  return Labs.find({courseId: courseId, status: {$ne: "hidden"}, removed: false, deadlineDate: {$lt: now}}, {sort: {deadlineDate: 1}});
};


computeOverallScore = function(labId, labTeamId) {
  console.log("computeOverallScore("+labId+","+labTeamId+")");
  var questionItems = ContentItems.find({containerId: labId, $or: [{kind: "question"}, {kind: "fileSubmission"}]}, {sort: {index: 1}});
  var totalScore = 0;
  questionItems.forEach(function(item) {
    //console.log("  item:", item);
    var rubric = Rubrics.findOne({itemId: item._id, removed: {$ne: true}});
    //console.log("  rubric:", rubric);
    //var contentItemSubmission = ContentItemSubmissions.findOne({itemId: item._id, labTeamId: labTeamId});
    //console.log("  contentItemSubmission:", contentItemSubmission);
    //var contentItemScore = ContentItemScores.findOne({contentItemId: item._id, teamId: labTeamId});
    //console.log("  contentItemScore:", contentItemScore);
    if (rubric) {
      var score = computeRubricPoints(rubric, labTeamId);
      //console.log("  score:", score);
      totalScore += score;
    }
  });
  return totalScore;
};

computeMaxOverallScore = function(labId) {
  console.log("computeMaxOverallScore("+labId+")");
  var questionItems = ContentItems.find({containerId: labId, $or: [{kind: "question"}, {kind: "fileSubmission"}]}, {sort: {index: 1}});
  var totalScore = 0;
  questionItems.forEach(function(item) {
    //console.log("  item:", item);
    var rubric = Rubrics.findOne({itemId: item._id, removed: {$ne: true}});
    //console.log("  rubric:", rubric);
    if (rubric) {
      var score = computeMaxRubricPoints(rubric);
      //console.log("  score:", score);
      totalScore += score;
    }
  });
  return totalScore;
};

computeRubricPoints = function(rubric, teamId) {
  console.log("computeRubricPoints(", rubric, teamId, ")");
  var categories = RubricCategories.find({rubricId: rubric._id, removed: {$ne: true}});
  var points = 0;
  categories.forEach(function(category) {
    points += computeCategoryPoints(category, teamId);
  });
  var contentItemScore = ContentItemScores.findOne({contentItemId: rubric.itemId, teamId: teamId});
  //console.log("contentItemScore:", contentItemScore);
  if (contentItemScore && contentItemScore.pointAdjustment) {
    points += parseFloat(contentItemScore.pointAdjustment);
  }
  //console.log("points: "+points);
  return points;
};

computeMaxRubricPoints = function(rubric) {
  console.log("computeMaxRubricPoints(", rubric, ")");
  var categories = RubricCategories.find({rubricId: rubric._id, removed: {$ne: true}});
  var points = 0;
  categories.forEach(function(category) {
    points += parseFloat(category.maxPoints);
  });
  //console.log("points: "+points);
  return points;
};

computeCategoryPoints = function(rubricCategory, teamId) {
  console.log("computeCategoryPoints("+rubricCategory+","+teamId+")");
  var maxPoints = parseFloat(rubricCategory.maxPoints);
  //console.log("maxPoints", maxPoints);
  var additive = rubricCategory.additive;
  //console.log("additive", additive);
  var enableBonusMalus = rubricCategory.enableBonusMalus;
  //console.log("enableBonusMalus", enableBonusMalus);

  var contentItemScore = ContentItemScores.findOne({contentItemId: rubricCategory.itemId, teamId: teamId});
  if (contentItemScore) {
    var checkedRubricItemIds = contentItemScore.checkedRubricItemIds;
    //console.log("checkedRubricItemIds", checkedRubricItemIds);
    var selectedRubricItems = RubricItems.find({rubricCategoryId: rubricCategory._id, removed: {$ne: true}, _id: {$in: checkedRubricItemIds}});
    //console.log(selectedRubricItems.fetch());
    var points = additive?0:maxPoints;
    selectedRubricItems.forEach(function(score) {
      if (additive) {
        points += parseFloat(score.points);
      } else {
        points -= parseFloat(score.points);
      }
    });
    if (!enableBonusMalus) {
      if (points<0) {
        points = 0;
      }
      if (points>maxPoints) {
        points = maxPoints;
      }
    }
    return points;
  } else {
    return 0;
  }
}
