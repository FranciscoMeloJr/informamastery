Meteor.methods({
	addTopicToBeChecked: function(topicId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a topic to be checked.");
		}
		var courseId = Topics.findOne({_id: topicId}).courseId;
		var toBeCheckedCount = TopicStatus.find({studentId: this.userId, courseId: courseId, toBeChecked: true}).count();
		var maxTopicsPerMasteryCheck = Courses.findOne({_id: courseId}).maxTopicsPerMasteryCheck;
		if (toBeCheckedCount>=maxTopicsPerMasteryCheck) {
			throw new Meteor.error("exceeds-max", "You can not check more than a given number of topics per check.");
		}
		var ownProblemCount = PracticeProblems.find({topicId: topicId, userId: this.userId, published:true, archived:false}).count();
		if (ownProblemCount===0) {
			throw new Meteor.error("no-practice-problem", "You cannot add a topic to be checked if you have not yet published a practice problem.");
		}
		// we could also check that all skills in this topic have been mastered, but let's not be so picky for now
		var now = new Date();
		FeedEvents.insert({userId: this.userId, courseId: courseId, date: now, active: true, event: "IAddedTopicToBeChecked", topicId: topicId});
		return TopicStatus.update({
			studentId: this.userId,
			courseId: courseId,
			topicId: topicId,
		}, {
			$set: {toBeChecked: true},
			$push: { log: {
				event: "SetToBeChecked",
				date: now,
			}},
		}, {
			upsert: true,
		});
	},

	removeTopicToBeChecked: function(topicId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a topic to be checked.");
		}
		var courseId = Topics.findOne({_id: topicId}).courseId;
		var now = new Date();
		FeedEvents.insert({userId: this.userId, courseId: courseId, date: now, active: true, event: "IRemovedTopicToBeChecked", topicId: topicId});
		return TopicStatus.update({
			studentId: this.userId,
			topicId: topicId,
		}, {
			$set: {toBeChecked: false},
			$push: { log: {
				event: "ClearToBeChecked",
				date: now,
			}},
		}, {
			upsert: true,
		});
	}
});
