Meteor.methods({
	startNewMasteryCheck: function(masteryCheckRegistrationId) {
    var now = new Date();
		console.log("Meteor.methods.startNewMasteryCheck("+masteryCheckRegistrationId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to start a mastery check with a student.");
		}
    var masteryCheckRegistration = MasteryCheckRegistrations.findOne({_id: masteryCheckRegistrationId});
    if (!masteryCheckRegistration) {
      throw new Meteor.Error("no-mastery-check-registration", "No such mastery check registration found.");
    }
		if (!isOnTeachingTeam(this.userId, masteryCheckRegistration.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to start a mastery check with a student.");
		}
    //TODO
		//if (StudentRegistrations.find({studentId: studentId, courseId: courseId}).count()===0) {
		//	throw new Meteor.Error("not-yet-registered", "This user is not yet registered as a student for this course.");
		//}
		var testerId = this.userId;
		var masteryCheckId = MasteryChecks.insert({
			masteryCheckRegistrationId: masteryCheckRegistrationId,
			studentIds: masteryCheckRegistration.studentIds,
			testerId: testerId,
      courseId: masteryCheckRegistration.courseId,
      labId: masteryCheckRegistration.labId,
			startDate: now,
			endDate: undefined,
			finished: false,
			notes: undefined,
			notesDate: undefined,
		});
		// create TopicChecks and SkillChecks for the to-be-checked Topics and Skills
		_.each(masteryCheckRegistration.topicIds, function(topicId) {
			var topicCheckId = TopicChecks.insert({
        studentIds: masteryCheckRegistration.studentIds,
				testerId: testerId,
        courseId: masteryCheckRegistration.courseId,
        labId: masteryCheckRegistration.labId,
				masteryCheckId: masteryCheckId,
				topicId: topicId,
				mastered: undefined,
				feedback: undefined,
				feedbackDate: undefined,
				endDate: undefined,
			});
			var ss = Skills.find({topicId: topicId});
			ss.forEach(function(skill) {
				var skillCheckId = SkillChecks.insert({
          studentIds: masteryCheckRegistration.studentIds,
					testerId: testerId,
          courseId: masteryCheckRegistration.courseId,
          labId: masteryCheckRegistration.labId,
					masteryCheckId: masteryCheckId,
					topicCheckId: topicCheckId,
					topicId: topicId,
					skillId: skill._id,
					mastered: undefined,
					date: undefined,
				});
			});
		});
		var result = MasteryCheckQueueEntries.remove({
      courseId: masteryCheckRegistration.courseId,
      masteryCheckRegistrationId: masteryCheckRegistrationId,
		});
		console.log("MasteryCheckQueueEntries.remove:", result);
		return masteryCheckId;
	},
	setSkillCheckMastered: function(skillCheckId, mastered) {
		console.log("Meteor.methods.setSkillCheckMastered("+skillCheckId+", "+mastered+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a skill check with a student.");
		}
		var check = SkillChecks.findOne({_id: skillCheckId});
		if (!check) {
			throw new Meteor.Error("check-does-not-exist", "The given skill check does not exist");
		}
		var masteryCheck = MasteryChecks.findOne({_id: check.masteryCheckId});
		if (masteryCheck.finished) {
			throw new Meteor.Error("check-finished", "This mastery check already has been finished.");
		}
		if (masteryCheck.reopened) {
			if (!isInstructor(this.userId, masteryCheck.courseId)) {
				throw new Meteor.Error("not-instructor", "You must be an instructor to update a reopened check.");
			}
		} else {
			if (check.testerId!=this.userId) {
				throw new Meteor.Error("not-user-starting-check", "You must be the same user who started this check to update it.");
			}
		}
		var now = new Date();
		return SkillChecks.update({_id: skillCheckId}, {$set: {
			mastered: mastered,
			date: now,
		}});
	},
	setTopicCheckFeedback: function(topicCheckId, feedback) {
		console.log("Meteor.methods.setTopicCheckFeedback("+topicCheckId+", "+feedback+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a topic check with a student.");
		}
		var check = TopicChecks.findOne({_id: topicCheckId});
		if (!check) {
			throw new Meteor.Error("check-does-not-exist", "The given topic check does not exist");
		}
		var masteryCheck = MasteryChecks.findOne({_id: check.masteryCheckId});
		if (masteryCheck.finished) {
			throw new Meteor.Error("check-finished", "This mastery check already has been finished.");
		}
		if (masteryCheck.reopened) {
			if (!isInstructor(this.userId, masteryCheck.courseId)) {
				throw new Meteor.Error("not-instructor", "You must be an instructor to update a reopened check.");
			}
		} else {
			if (check.testerId!=this.userId) {
				throw new Meteor.Error("not-user-starting-check", "You must be the same user who started this check to update it.");
			}
		}
		var now = new Date();
		return TopicChecks.update({_id: topicCheckId}, {$set: {
			feedback: feedback,
			feedbackDate: now,
		}});
	},
	setMasteryCheckNotes: function(masteryCheckId, notes) {
		console.log("Meteor.methods.setMasteryCheckNotes("+masteryCheckId+", "+notes+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a mastery check with a student.");
		}
		var check = MasteryChecks.findOne({_id: masteryCheckId});
		if (!check) {
			throw new Meteor.Error("check-does-not-exist", "The given mastery check does not exist");
		}
		if (check.finished) {
			throw new Meteor.Error("check-finished", "This mastery check already has been finished.");
		}
		if (check.reopened) {
			if (!isInstructor(this.userId, check.courseId)) {
				throw new Meteor.Error("not-instructor", "You must be an instructor to update a reopened check.");
			}
		} else {
			if (check.testerId!=this.userId) {
				throw new Meteor.Error("not-user-starting-check", "You must be the same user who started this check to update it.");
			}
		}
		var now = new Date();
		return MasteryChecks.update({_id: masteryCheckId}, {$set: {
			notes: notes,
			notesDate: now,
		}});
	},
	finishMasteryCheck: function(masteryCheckId) {
		console.log("Meteor.methods.finishMasteryCheck("+masteryCheckId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to finish a mastery check with a student.");
		}
		var check = MasteryChecks.findOne({_id: masteryCheckId});
		if (!check) {
			throw new Meteor.Error("check-does-not-exist", "The given mastery check does not exist");
		}
		if (check.finished) {
			throw new Meteor.Error("check-finished", "This mastery check already has been finished.");
		}
		if (check.reopened) {
			if (!isInstructor(this.userId, check.courseId)) {
				throw new Meteor.Error("not-instructor", "You must be an instructor to finish a reopened check.");
			}
		} else {
			if (check.testerId!=this.userId) {
				throw new Meteor.Error("not-user-starting-check", "You must be the same user who started this check to finish it.");
			}
		}
		var now = new Date();
		MasteryCheckRegistrations.remove({_id: check.masteryCheckRegistrationId});
		// for each TopicCheck in this MasteryCheck
		TopicChecks.find({masteryCheckId: masteryCheckId}).forEach(function(topicCheck) {
			var topicMastered = true;
			// for each SkillCheck in this TopicCheck
			SkillChecks.find({topicCheckId: topicCheck._id}).forEach(function(skillCheck) {
				if (!skillCheck.mastered) {
					topicMastered = false;
					// if we couldn't convince the tester that we mastered this skill, clear its readyness
          //TODO
					SkillStatus.update({
						studentId: skillCheck.studentId,
						topicId: skillCheck.topicId,
						skillId: skillCheck.skillId,
					}, {
						$set: {ready: false},
						$push: { log: {
							event: "ClearReadyDueToFailedMasteryCheck",
							date: now,
						}},
					});
				}
			});
			// finish TopicCheck
			TopicChecks.update({
				_id: topicCheck._id
			}, {
				$set: {
					endDate: now,
					mastered: topicMastered,
				},
			});
			// update TopicStatus accordingly
      //TODO
			if (topicMastered) {
				TopicStatus.update({
					studentId: check.studentId,
					topicId: topicCheck.topicId
				}, {
					$set: {
						toBeChecked: false,
						mastered: true,
						masteredDate: now,
					},
					$push: {
						log: {
							event: "Mastered",
							date: now,
						},
					},
				});
			} else {
				TopicStatus.update({
					studentId: check.studentId,
					topicId: topicCheck.topicId
				}, {
					$set: {
						toBeChecked: false,
						mastered: false,
					},
					$push: {
						log: {
							event: "FailedMasteryCheck",
							date: now,
						},
					},
				});
			}
		});
		// finish MasteryCheck
		var propertiesToSet = {};
		if (check.reopened) {
			propertiesToSet.finished = true;
			propertiesToSet.reopened = false;
			propertiesToSet.lastOverrideDate = now;
		} else {
			propertiesToSet.finished = true;
			propertiesToSet.endDate = now;
		}
		return MasteryChecks.update({_id: masteryCheckId}, {$set: propertiesToSet});
	},
	reopenMasteryCheck: function(masteryCheckId) {
		console.log("Meteor.methods.reopenMasteryCheck("+masteryCheckId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to reopen a mastery check.");
		}
		var check = MasteryChecks.findOne({_id: masteryCheckId});
		if (!check) {
			throw new Meteor.Error("check-does-not-exist", "The given mastery check does not exist");
		}
		if (!isInstructor(this.userId, check.courseId)) {
			throw new Meteor.Error("not-instructor", "You must be an instructor to reopen this check.");
		}
		var now = new Date();
		return MasteryChecks.update({_id: masteryCheckId}, {$set: {
			reopened: true,
			finished: false,
		}});
	},
	deleteMasteryCheck: function(masteryCheckId) {
		console.log("Meteor.methods.deleteMasteryCheck("+masteryCheckId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to delete a mastery check.");
		}
		var check = MasteryChecks.findOne({_id: masteryCheckId});
		if (!check) {
			throw new Meteor.Error("check-does-not-exist", "The given mastery check does not exist");
		}
		if (!isInstructor(this.userId, check.courseId)) {
			throw new Meteor.Error("not-instructor", "You must be an instructor to delete this check.");
		}
		var now = new Date();
		var topicIds = [];
		var studentId = check.studentId; //TODO
		// for each TopicCheck in this MasteryCheck
		TopicChecks.find({masteryCheckId: masteryCheckId}).forEach(function(topicCheck) {
			topicIds.push(topicCheck.topicId);
			// for each SkillCheck in this TopicCheck
			SkillChecks.find({topicCheckId: topicCheck._id}).forEach(function(skillCheck) {
				SkillChecks.update({
					_id: skillCheck._id
				}, {
					deleted: true,
					deletionDate: now,
					deletedBy: this.userId,
					original: skillCheck,
				});
			});
			TopicChecks.update({
				_id: topicCheck._id
			}, {
				deleted: true,
				deletionDate: now,
				deletedBy: this.userId,
				original: topicCheck,
			});
		});
		MasteryChecks.update({_id: masteryCheckId}, {
			deleted: true,
			deletionDate: now,
			deletedBy: this.userId,
			original: check,
		});
    //TODO
		// recalculate TopicStatus (based on all the remaining TopicChecks of the student)
		TopicStatus.find({studentId: studentId, topicId: {$in: topicIds}}).forEach(function(topicStatus) {
			// this topic is mastered if we find some other TopicCheck (besides the one we just deleted) where it was mastered,
			// otherwise it is NOT mastered
			var mastered = TopicChecks.find({studentId: studentId, topicId: topicStatus.topicId, mastered: true}).count()>0;
			TopicStatus.update({studentId: studentId, topicId: topicStatus.topicId}, {
				$set: {
					mastered: mastered
				},
				$push: {
					log: {
						event: "MasteryCheckDeletedSoUpdatedPotentiallyAffectedTopicStatus",
						date: now,
					},
				},
			});
		});
	},
});
