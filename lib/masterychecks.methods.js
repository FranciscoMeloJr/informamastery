Meteor.methods({
	startMasteryCheck: function(studentId, courseId, topicIds) {
		console.log("Meteor.methods.startMasteryCheck("+studentId+", "+courseId+", "+topicIds+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to start a mastery check with a student.");
		}
		if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to start a mastery check with a student.");
		}
		if (StudentRegistrations.find({studentId: studentId, courseId: courseId}).count()===0) {
			throw new Meteor.Error("not-yet-registered", "This user is not yet registered as a student for this course.");
		}
		var now = new Date();
		var testerId = this.userId;
		var masteryCheckId = MasteryChecks.insert({
			studentId: studentId,
			testerId: testerId,
			courseId: courseId,
			startDate: now,
			endDate: undefined,
			finished: false,
			notes: undefined,
			notesDate: undefined,
		});
		// create TopicChecks and SkillChecks for the to-be-checked Topics and Skills
		var tss = TopicStatus.find({courseId: courseId, studentId: studentId, toBeChecked: true});
		tss.forEach(function(topicStatus) {
			var topicCheckId = TopicChecks.insert({
				studentId: studentId,
				testerId: testerId,
				courseId: courseId,
				masteryCheckId: masteryCheckId,
				topicId: topicStatus.topicId,
				mastered: undefined,
				feedback: undefined,
				feedbackDate: undefined,
				endDate: undefined,
			});
			var ss = Skills.find({topicId: topicStatus.topicId});
			ss.forEach(function(skill) {
				var skillCheckId = SkillChecks.insert({
					studentId: studentId,
					testerId: testerId,
					courseId: courseId,
					masteryCheckId: masteryCheckId,
					topicCheckId: topicCheckId,
					topicId: topicStatus.topicId,
					skillId: skill._id,
					mastered: undefined,
					date: undefined,
				});
			});
		});
		return masteryCheckId;
	},
});
