Meteor.methods({
  scoreSetRubricItemState: function(contentItemId, teamId, rubricItemId, selected) {
    var now = new Date();
		logMethodCall(now, "scoreSetRubricItemState", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to set the rubric item state.");
		}
    var team = LabTeams.findOne({_id: teamId});
		if (!team) {
			throw new Meteor.Error("team-does-not-exist", "The specified team does not exist.");
		}
    var rubricItem = RubricItems.findOne({_id: rubricItemId});
		if (!rubricItem) {
			throw new Meteor.Error("rubric-item-does-not-exist", "The specified rubric item does not exist.");
		}
    var update = {
      $set: {
        contentItemId: contentItemId,
        teamId: teamId,
        userIds: team.memberIds, // maintain these for pub/sub on userId
        courseId: contentItem.courseId,
        rubricId: rubricItem.rubricId,
        lastModificationDate: now,
  			modifierId: this.userId,
      },
      $push : {
        log: [{what: "SetRubricItemState", when: now, who: this.userId, rubricItemId: rubricItemId, selected: selected}],
      }
    };
    if (selected) {
      update.$addToSet = {checkedRubricItemIds: rubricItemId};
    } else {
      update.$pullAll  = {checkedRubricItemIds: [rubricItemId]};
    }
    return ContentItemScores.update({
      contentItemId: contentItemId,
      teamId: teamId,
    }, update, {
      upsert: true,
		});
  },
  scoreSetPointAdjustment: function(contentItemId, teamId, pointAdjustment) {
    var now = new Date();
		logMethodCall(now, "scoreSetPointAdjustment", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to set the point adjustment.");
		}
    var rubric = Rubrics.findOne({itemId: contentItemId});
    if (!rubric) {
      throw new Meteor.Error("rubric-does-not-exist", "There is no rubric for this content item.");
    }
    var team = LabTeams.findOne({_id: teamId});
		if (!team) {
			throw new Meteor.Error("team-does-not-exist", "The specified team does not exist.");
		}
    return ContentItemScores.update({
      contentItemId: contentItemId,
      teamId: teamId,
    }, {
      $set: {
        contentItemId: contentItemId,
        teamId: teamId,
        userIds: team.memberIds, // maintain these for pub/sub on userId
        courseId: contentItem.courseId,
        rubricId: rubric._id,
        lastModificationDate: now,
  			modifierId: this.userId,
        pointAdjustment: pointAdjustment,
      },
      $push : {
        log: [{what: "SetPointAdjustment", when: now, who: this.userId, pointAdjustment: pointAdjustment}],
      }
    }, {
      upsert: true,
		});
  },
  scoreSetComment: function(contentItemId, teamId, comment) {
    var now = new Date();
		logMethodCall(now, "scoreSetComment", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to set the comment.");
		}
    var rubric = Rubrics.findOne({itemId: contentItemId});
    if (!rubric) {
      throw new Meteor.Error("rubric-does-not-exist", "There is no rubric for this content item.");
    }
    var team = LabTeams.findOne({_id: teamId});
		if (!team) {
			throw new Meteor.Error("team-does-not-exist", "The specified team does not exist.");
		}
    return ContentItemScores.update({
      contentItemId: contentItemId,
      teamId: teamId,
    }, {
      $set: {
        contentItemId: contentItemId,
        teamId: teamId,
        userIds: team.memberIds, // maintain these for pub/sub on userId
        courseId: contentItem.courseId,
        rubricId: rubric._id,
        lastModificationDate: now,
  			modifierId: this.userId,
        comment: comment,
      },
      $push : {
        log: [{what: "SetComment", when: now, who: this.userId, comment: comment}],
      }
    }, {
      upsert: true,
		});
  },
});
