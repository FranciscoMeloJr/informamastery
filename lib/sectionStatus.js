Meteor.methods({
	startReadingSection: function(sectionId) {
		var now = new Date();
		logMethodCall(now, "startReadingSection", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update the section status.");
		}
		var section = Sections.findOne({_id: sectionId});
		if (!section) {
			throw new Meteor.Error("unknown-section", "There is no section with the given id");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.startReadingSection: sectionId:", sectionId, "userId:", this.userId, "date:", now);
		SectionStatus.update({
			userId: this.userId,
			courseId: section.courseId,
			bookId: section.bookId,
			chapterId: section.chapterId,
			sectionId: sectionId,
		}, {
			$set: {
				startedReading: true,
				startedReadingDate: now,
			},
			$push: {
				log: {what: "StartedReading", when: now, who: this.userId},
			},
		}, {
			upsert: true,
		});
		var sectionStatusId = SectionStatus.findOne({
			userId: this.userId,
			courseId: section.courseId,
			sectionId: sectionId,
		})._id;
		FeedEvents.insert({
			userId: this.userId,
			courseId: section.courseId,
			date: now,
			active: true,
			event: "IStartedReadingTextbookSection",
			sectionId: section._id,
			sectionStatusId: sectionStatusId,
		});
		AnalyticsLog.insert({
			userId: this.userId,
			courseId: section.courseId,
			serverDate: now,
			event: "startReadingSection",
			contents: {
				sectionId: section._id,
				sectionStatusId: sectionStatusId,
			},
		});
		return sectionStatusId;
	},
	finishReadingSection: function(sectionId) {
		var now = new Date();
		logMethodCall(now, "finishedReadingSection", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update the section status.");
		}
		var section = Sections.findOne({_id: sectionId});
		if (!section) {
			throw new Meteor.Error("unknown-section", "There is no section with the given id");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.finishReadingSection: sectionId:", sectionId, "userId:", this.userId, "date:", now);
		SectionStatus.update({
			userId: this.userId,
			courseId: section.courseId,
			bookId: section.bookId,
			chapterId: section.chapterId,
			sectionId: sectionId,
		}, {
			$set: {
				finishedReading: true,
				finishedReadingDate: now,
			},
			$push: {
				log: {what: "FinishedReading", when: now, who: this.userId},
			},
		}, {
			upsert: true,
		});
		var sectionStatusId = SectionStatus.findOne({
			userId: this.userId,
			courseId: section.courseId,
			sectionId: sectionId,
		})._id;
		FeedEvents.insert({
			userId: this.userId,
			courseId: section.courseId,
			date: now,
			active: true,
			event: "IFinishedReadingTextbookSection",
			sectionStatusId: sectionStatusId,
			sectionId: section._id,
		});
		AnalyticsLog.insert({
			userId: this.userId,
			courseId: section.courseId,
			serverDate: now,
			event: "finishReadingSection",
			contents: {
				sectionId: section._id,
				sectionStatusId: sectionStatusId,
			},
		});
		return sectionStatusId;
	},
	updateSectionStatus: function(sectionId, recallText) {
		var now = new Date();
		logMethodCall(now, "updateSectionStatus", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update the section status.");
		}
		var section = Sections.findOne({_id: sectionId});
		if (!section) {
			throw new Meteor.Error("unknown-section", "There is no section with the given id");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.updateSectionStatus: sectionId:", sectionId, "userId:", this.userId, "date:", now, "recallText:", recallText);
		SectionStatus.update({
			userId: this.userId,
			courseId: section.courseId,
			bookId: section.bookId,
			chapterId: section.chapterId,
			sectionId: sectionId,
		}, {
			$set: {
				recallText: recallText,
				modificationDate: now,
			},
			$push: {
				log: {what: "Saved", when: now, who: this.userId, recallText: recallText},
			},
		}, {
			upsert: true,
		});
		var sectionStatusId = SectionStatus.findOne({
			userId: this.userId,
			courseId: section.courseId,
			sectionId: sectionId,
		})._id;
		FeedEvents.insert({
			userId: this.userId,
			courseId: section.courseId,
			date: now,
			active: true,
			event: "IRecalledTextbookSectionInformation",
			sectionStatusId: sectionStatusId,
			sectionId: section._id,
			recallText: recallText,
		});
		AnalyticsLog.insert({
			userId: this.userId,
			courseId: section.courseId,
			serverDate: now,
			event: "updateSectionRecallText",
			contents: {
				sectionId: section._id,
				sectionStatusId: sectionStatusId,
				recallText: recallText,
			},
		});
		return sectionStatusId;
	},
});
