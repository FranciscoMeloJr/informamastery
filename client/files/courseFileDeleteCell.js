Template.courseFileDeleteCell.events({
  'click .del-file': function(e, t) {
    console.log("Template.courseFileDeleteCell.events:click .del-file");
    //TODO: maybe call a meteor method instead of using a remove query?
    CourseFiles.remove({_id: this._id});
  },
});
