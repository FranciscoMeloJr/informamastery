Template.skillEditPage.events({
	'submit .form': function (event) {
		var skillId = this.skill._id;
		var courseId = this.course._id;
		var title = event.target.title.value;
		var description = event.target.description.value;
		Meteor.call('updateSkill', skillId, title, description, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("skillPage", {courseId: courseId, skillId: skillId});
			}
		});
		// Prevent default form submit
		return false;
	},
});
