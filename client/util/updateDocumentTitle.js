// ensure that normal templates don't depend on this
// just use it for keeping the document title in sync

Template.header.helpers({
	course: function() {
		return Session.get('course');
	},
});

Deps.autorun(function() {
	var course = Session.get("course");
	if (course) {
		document.title = course.abbreviation+" "+course.instance+" - Informa";
	} else {
		document.title = "Informa";
	}
});
