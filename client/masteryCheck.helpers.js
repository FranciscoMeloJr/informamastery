Template.masteryCheckPage.created = function() {
	console.log("Template.masteryCheckPage.created");
	var pageCreated = moment(new Date());
	this.timer = Meteor.setInterval(function() {
		var now = moment(new Date());
		var minutes = now.diff(pageCreated, 'minutes');
		Session.set("minutesSincePageCreated", minutes);
	}, 1000);
};

Template.masteryCheckPage.destroyed = function() {
	console.log("Template.masteryCheckPage.destroyed");
	Meteor.clearInterval(this.timer);
};

Template.masteryCheckPage.rendered = function() {
	console.log("Template.masteryCheckPage.rendered");
    var moment1 = moment(new Date());
    console.log(moment1.format("dddd, MMMM Do YYYY, H:mm:ss"));
    var moment2 = moment(new Date());
    console.log(moment2.format("dddd, MMMM Do YYYY, H:mm:ss"));
    var diffInMinutes = moment1.diff(moment2, 'minutes');
    console.log(diffInMinutes);
};


Template.masteryCheckPage.helpers({
	getMinutesSincePageRendered: function() {
		return Session.get("minutesSincePageCreated");
	},
	getTopicChecksForMasteryCheck: function(masteryCheckId) {
		return TopicChecks.find({masteryCheckId: masteryCheckId});
	},
});

Template.topicCheckTablePart.helpers({
	getSkillChecksForTopicCheck: function(topicCheckId) {
		return SkillChecks.find({topicCheckId: topicCheckId});
	},
});

Template.weekStudentMatrixPage.helpers({
	getWeeksInCourse: function(courseId) {
		var course = Courses.findOne({_id: courseId});
		var weeksInCourse = moment(course.endDate).diff(moment(course.startDate), "weeks");
		var weeksArray = [];
		for (var week=1; week<=weeksInCourse; week++) {
			weeksArray.push({number: week});
		}
		return weeksArray;
	},
	getTopicStudentMatrixRows: function(courseId) {
		var course = Courses.findOne({_id: courseId});
		var weeksInCourse = moment(course.endDate).diff(moment(course.startDate), "weeks");
		var studentRegistrations = StudentRegistrations.find({courseId: courseId});
		var rows = studentRegistrations.map(function(studentRegistration) {
			var row = {
				studentId: studentRegistration.studentId,
				weeks: [],
			};
			for (var week=1; week<=weeksInCourse; week++) {
				row.weeks.push({
					number: week,
					checkInfos: [],
				});
			}
			var masteryChecks = MasteryChecks.find({studentId: studentRegistration.studentId});
			masteryChecks.forEach(function(masteryCheck) {
				var weekOfCheck = moment(masteryCheck.startDate).diff(moment(course.startDate), "weeks");
				var checkInfo = {
					masteryCheck: masteryCheck,
					masteredTopicCount: 0,
					notYetMasteredTopicCount: 0,
				};
				row.weeks[weekOfCheck].checkInfos.push(checkInfo);
				TopicChecks.find({masteryCheckId: masteryCheck._id}).forEach(function(topicCheck) {
					if (topicCheck.mastered) {
						checkInfo.masteredTopicCount++;
					} else {
						checkInfo.notYetMasteredTopicCount++;
					}
				});

			});
			return row;
		});
		return rows;
	},

});
