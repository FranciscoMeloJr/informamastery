function getTopicsForMisconception(misconception) {
  var topicIds = misconception.topicIds;
  if (!topicIds) {
    topicIds = [];
  }
  return Topics.find({_id: {$in: topicIds}}, {sort: {masteryRequiredForGrade: 1}});
}


Template.misconceptionPage.helpers({
	getTopicCountForMisconception: function(misconception) {
    console.log("getTopicCountForMisconception(misconception:", misconception, ")");
    return getTopicsForMisconception(misconception).count();
	},
  getTopicsForMisconception: function(misconception) {
    console.log("getTopicsForMisconception(misconception:", misconception, ")");
    return getTopicsForMisconception(misconception);
	},
  getSkillsForMisconceptionAndTopic: function(misconception, topicId) {
    console.log("getSkillsForMisconceptionAndTopic(misconception:", misconception, "topicId="+topicId+")");
    var skillIds = misconception.skillIds;
    if (!skillIds) {
      skillIds = [];
    }
    console.log(skillIds);
    return Skills.find({topicId: topicId, _id: {$in: skillIds}});
  },
  getStudyTasksForMisconceptionAndTopic: function(misconception, topicId) {
    console.log("getStudyTasksForMisconceptionAndTopic(misconception:", misconception, "topicId="+topicId+")");
    var studyTaskIds = misconception.studyTaskIds;
    if (!studyTaskIds) {
      studyTaskIds = [];
    }
    console.log(studyTaskIds);
    return StudyTasks.find({topicId: topicId, _id: {$in: studyTaskIds}});
  },
  getPracticeProblemsForMisconceptionAndTopic: function(misconception, topicId) {
    console.log("getPracticeProblemsForMisconceptionAndTopic(misconception:", misconception, "topicId="+topicId+")");
    var practiceProblemIds = misconception.practiceProblemIds;
    if (!practiceProblemIds) {
      practiceProblemIds = [];
    }
    console.log(practiceProblemIds);
    return PracticeProblems.find({topicId: topicId, _id: {$in: practiceProblemIds}});
  },
});
