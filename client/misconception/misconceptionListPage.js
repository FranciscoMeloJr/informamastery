Template.misconceptionListPage.helpers({
  getMisconceptionCountForCourse: function(courseId) {
    return Misconceptions.find({courseId: courseId, removed: {$ne: true}}).count();
  },
  getMisconceptionsForCourse: function(courseId) {
    return Misconceptions.find({courseId: courseId, removed: {$ne: true}});
  },
  countMisconceptionsWithoutTopic: function(courseId) {
    return Misconceptions.find({courseId: courseId, $or: [{topicIds: null}, {topicIds: {$size: 0}}], removed: {$ne: true}}).count();
  },
  getMisconceptionsWithoutTopic: function(courseId) {
    return Misconceptions.find({courseId: courseId, $or: [{topicIds: null}, {topicIds: {$size: 0}}], removed: {$ne: true}});
  },
  getTopicsForCourse: function(courseId) {
    console.log("getTopicsForCourse("+courseId+")");
		return Topics.find({courseId: courseId, removed: {$ne: true}}, {sort: {masteryRequiredForGrade: 1}});
	},
  countMisconceptionsForTopic: function(topicId) {
    return Misconceptions.find({topicIds: topicId, removed: {$ne: true}}).count();
  },
  getMisconceptionsForTopic: function(topicId) {
    return Misconceptions.find({topicIds: topicId, removed: {$ne: true}});
  },
});
