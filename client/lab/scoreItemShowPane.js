Template.scoreItemShowPane.helpers({
  isSelected: function(contentItemId, teamId, rubricItemId) {
    return ContentItemScores.find({contentItemId: contentItemId, teamId: teamId, checkedRubricItemIds: rubricItemId}).count()>0;
  },
});
