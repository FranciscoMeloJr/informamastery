Template.labGradingPage.helpers({
	getTeams: function(labId) {
    if (labId) {
      return LabTeams.find({labId: labId, removed: {$ne: true}}, {sort: {creationDate: 1}});
    } else {
      return undefined;
    }
  },
  getQuestionItems: function(labId) {
		return ContentItems.find({containerId: labId, $or: [{kind: "question"}, {kind: "fileSubmission"}]}, {sort: {index: 1}});
	},
  getUploadCount: function() {
    console.log("Template.labGradingPage.helpers:getUploadCount");
    //console.log(this);
    var labTeam = this;
    console.log("labTeam:", labTeam);
    var uploadCount = LabSubmissionFiles.find({"metadata.labTeamId": labTeam._id}).count();
    console.log("uploadCount:", uploadCount);
    return uploadCount;
  },
  getSubmissionCount: function() {
    console.log("Template.labGradingPage.helpers:getSubmissionCount");
    //console.log(this);
    var labTeam = this;
    return ContentItemSubmissions.find({labTeamId: labTeam._id}).count();
  },
  getItemUploadCount: function() {
    console.log("Template.labGradingPage.helpers:getItemUploadCount");
    //console.log(this);
    var labItem = this;
    console.log("labItem:", labItem);
    var uploadCount = LabSubmissionFiles.find({"metadata.labItemId": labItem._id}).count();
    console.log("uploadCount:", uploadCount);
    return uploadCount;
  },
  getItemSubmissionCount: function() {
    console.log("Template.labGradingPage.helpers:getItemSubmissionCount");
    //console.log(this);
    var labItem = this;
    return ContentItemSubmissions.find({itemId: labItem._id}).count();
  },
});
