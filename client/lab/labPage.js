Template.labPage.helpers({
	getTopics: function(topicIds) {
		topicIds = topicIds || [];
		return Topics.find({_id: {$in: topicIds}}, {sort: {masteryRequiredForGrade: 1}});
	},
	getTheme: function(themeId) {
		return Themes.findOne({_id: themeId});
	},
	getItems: function(labId) {
		return ContentItems.find({containerId: labId}, {sort: {index: 1}});
	},
	kindIs: function(k) {
		return this.kind===k;
	},
	getNumberOfQuestionItems: function(labId) {
		return ContentItems.find({containerId: labId, kind: "question"}).count();
	},
	getNumberOfOwnSubmissions: function(labId) {
		var itemIds = ContentItems.find({containerId: labId, kind: "question"}).map(function (item) {return item._id;});
		return ContentItemSubmissions.find({itemId: {$in: itemIds}, userIds: Meteor.userId()}).count();
	},
});


Template.labPage.events({
});
