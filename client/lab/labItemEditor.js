Template.labItemEditor.created = function () {
	console.log("Template.labItemEditor.created()");
	console.log("this");
	console.log(this);
	console.log("this.data");
	console.log(this.data);
	// ReactiveVar to hold the editing state
	this.isEditing = new ReactiveVar(this.data?this.data.isNewlyCreated:false);
};


Template.labItemEditor.rendered = function(){
	this.$('.insert-item-dropdown').dropdown();
	this.$('.initially-focused').focus();
};


Template.labItemEditor.helpers({
	isEditing: function() {
		// retrieve the isEditing state from the ReactiveVar set below
		return Template.instance().isEditing.get();
	},
	kindIs: function(k) {
		return this.kind===k;
	},
	typeIs: function(t) {
		return this.type===t;
	},
});


var save = function(contentItem, form) {
	// save changes
	if (contentItem.kind==="section" || contentItem.kind==="subsection" || contentItem.kind==="subsubsection") {
		var title = form.find("input[name='title']").val();
		console.log(title);
		Meteor.call("updateContentItemTitle", contentItem._id, title, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	} else if (contentItem.kind==="markdown" || contentItem.kind==="mathjax") {
		var text = form.find("textarea[name='text']").val();
		console.log(text);
		Meteor.call("updateContentItemText", contentItem._id, text, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	} else if (contentItem.kind==="code") {
		var lang = form.find("input[name='lang']").val();
		var text = form.find("textarea[name='text']").val();
		console.log(text);
		Meteor.call("updateContentItemLangAndText", contentItem._id, lang, text, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	} else if (contentItem.kind==="question") {
		var type = "textarea";
		var text = form.find("textarea[name='text']").val();
		console.log(text);
		var contents = form.find("textarea[name='contents']").val();
		console.log(contents);
		Meteor.call("updateQuestionContentItem", contentItem._id, type, text, contents, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	} else if (contentItem.kind==="question-flowchart") {
		var type = "flowchart";
		var text = form.find("textarea[name='text']").val();
		console.log(text);
		var contents = ""; //TODO. Get initial answer from JointJS. form.find("textarea[name='contents']").val();
		console.log(contents);
		Meteor.call("updateQuestionContentItem", contentItem._id, type, text, contents, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	} else if (contentItem.kind==="image") {
			var url = form.find("input[name='url']").val();
			console.log(url);
			Meteor.call("updateContentItemUrl", contentItem._id, url, function(error, result) {
				if (error) {
					throwError(error.reason);
				}
			});
	} else if (contentItem.kind==="fileSubmission") {
		console.log("TODO: save content item of kind fileSubmission");
		//TODO
	} else {
		//TODO: handle all other kinds
	}
};


Template.labItemEditor.events({
	'click .edit-item-button': function(event, template) {
		console.log("click .edit-item-button");
		var isEditing = template.isEditing.get();
		var contentItem = this;
		if (isEditing) {
			var item = $(event.target).closest(".editable-item");
			var form = item.find("form");
			save(contentItem, form);
		}
		// store the isEditing state in the ReactiveVar retrieved above
		template.isEditing.set(!isEditing);
	},
	'submit form': function(event, template) {
		console.log("submit form");
		var contentItem = this;
		var form = $(event.target);
		save(contentItem, form);
		template.isEditing.set(false);
		// prevent default
		return false;
	},
	'click .remove-item-button': function(event, template) {
		var contentItem = this;
		Meteor.call("removeContentItem", contentItem._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .insert-item': function(event, template) {
		console.log("click .insert-item");
		var containerId = this.containerId;
		console.log("event:");
		console.log(event);
		var target = $(event.target);
		console.log("target:");
		console.log(target);
		var kind = target.attr("data-kind");
		console.log("kind:");
		console.log(kind);

		// We need to get the DOM node for this item, so we can find the previous and next items
		var item = $(event.target).closest(".editable-item");
		console.log("containing item:");
		console.log(item);
		// get the html element (in which the insert button was pressed) and the one before and after it
		var el = item.get(0);
		var after = item.next().get(0);
		var newIndex;

		// Blaze.getData takes as a parameter an html element
		// and returns the data context that was bound when that html element was rendered
		if (!after) {
			// if it was dragged into the last position grab the
			// previous element's data context and add one to the position
			newIndex = Blaze.getData(el).index + 1;
		} else {
			// else take the average of the two positions of the previous
			// and next elements
			newIndex = (Blaze.getData(el).index + Blaze.getData(after).index) / 2;
		}
		Meteor.call("addContentItem", containerId, "Labs", kind, newIndex, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
