Template.scoreShowPane.helpers({
  hasRubric: function(contentItemId) {
		return Rubrics.find({itemId: contentItemId}).count()>0;
	},
	getRubric: function(contentItemId) {
		return Rubrics.findOne({itemId: contentItemId, removed: {$ne: true}});
	},
  getRubricCategories: function(rubricId) {
    return RubricCategories.find({rubricId: rubricId, removed: {$ne: true}}, {sort: {index: 1}});
  },
  getContentItemScore: function(contentItemId, teamId) {
    return ContentItemScores.findOne({contentItemId: contentItemId, teamId: teamId});
  },
  computeRubricPoints: function(rubric, teamId) {
    return computeRubricPoints(rubric, teamId);
  },
  computeMaxRubricPoints: function(rubric) {
    return computeMaxRubricPoints(rubric);
  },
  multipleCateogoriesOrPointAdjustment: function(rubric, pointAdjustment) {
    return pointAdjustment || RubricCategories.find({rubricId: rubric._id, removed: {$ne: true}}).count()>1;
  },
});
