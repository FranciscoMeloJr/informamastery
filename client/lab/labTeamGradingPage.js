Template.labTeamGradingPage.helpers({
	getQuestionItems: function(labId) {
		return ContentItems.find({containerId: labId, $or: [{kind: "question"}, {kind: "fileSubmission"}]}, {sort: {index: 1}});
	},
	getTeamSubmission: function(teamId) {
		// get the submission for this item made by our current lab team
		var item = this;
		console.log("getTeamSubmission("+teamId+") item:", item);
		var lab = Labs.findOne({_id: item.containerId});
		console.log("getTeamSubmission() lab:", lab);
		var labTeam = LabTeams.findOne({_id: teamId});
		console.log("getTeamSubmission() labTeam:", labTeam);
		if (labTeam) {
			return ContentItemSubmissions.findOne({itemId: item._id, labTeamId: labTeam._id});
		} else {
			return undefined;
		}
	},
	getLabSubmissionFiles: function(teamId, itemId) {
    return LabSubmissionFiles.find({"metadata.labItemId": itemId, "metadata.labTeamId": teamId});
  },
  countLabSubmissionFiles: function(teamId, itemId) {
    return LabSubmissionFiles.find({"metadata.labItemId": itemId, "metadata.labTeamId": teamId}).count();
  },
	getLabSubmissionImageFiles: function(teamId, itemId) {
    return LabSubmissionFiles.find({"metadata.labItemId": itemId, "metadata.labTeamId": teamId, $or: [{contentType: "image/jpeg"}, {contentType: "image/png"}, {contentType: "image/gif"}]});
  },
  countLabSubmissionImageFiles: function(teamId, itemId) {
    return LabSubmissionFiles.find({"metadata.labItemId": itemId, "metadata.labTeamId": teamId, $or: [{contentType: "image/jpeg"}, {contentType: "image/png"}, {contentType: "image/gif"}]}).count();
  },
  link: function() {
    //console.log('labSubmissionFileList.helpers.link');
    return LabSubmissionFiles.baseURL + "/md5/" + this.md5;
  },
  shortFilename: function(w) {
    //console.log('labSubmissionFileList.helpers.shortFilename');
    if (!w) {
      w = 16;
    }
    if (this.filename && this.filename.length) {
      return shortenFileName(this.filename, w);
    } else {
      return "(no filename)";
    }
  },
  owner: function() {
    return this.metadata ? this.metadata.ownerId : undefined;
  },
  formattedLength: function() {
    return numeral(this.length).format('0.0b');
  },
});


Template.labTeamGradingPage.events({
});

// HACK: Just copied this from labSubmissionFileList
function shortenFileName(name, w) {
  if (!w) {
    w = 16;
  }
  w += w % 4;
  w = (w-4)/2
  if (name.length > 2*w) {
    return name.substring(0, w) + '…' + name.substring(name.length-w, name.length);
  } else {
    return name;
  }
}
