Template.labCard.helpers({
	countQuestionItems: function(labId) {
		return ContentItems.find({containerId: labId, kind: "question"}).count();
	},
	countOwnSubmissions: function(labId) {
		var itemIds = ContentItems.find({containerId: labId, kind: "question"}).map(function (item) {return item._id;});
		return ContentItemSubmissions.find({itemId: {$in: itemIds}, userIds: Meteor.userId()}).count();
	},
});
