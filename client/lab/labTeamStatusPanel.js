Template.labTeamStatusPanel.helpers({
  isPreviewOrOpenOrClosed: function(status) {
    return status=="preview" || status=="open" || status=="closed";
  },
	getNumberOfLabTeams: function(labId) {
    return LabTeams.find({labId: labId, removed: {$ne: true}}).count();
  },
  countEligibleStudents: function(lab) {
    var studentIds = StudentRegistrations.find({courseId: lab.courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
    return studentIds.length;
  },
  countAssignedStudents: function(lab) {
    var assignedStudentIds = [];
    LabTeams.find({labId: lab._id, removed: {$ne: true}}).forEach(function(labTeam) {
      assignedStudentIds = assignedStudentIds.concat(labTeam.memberIds);
    });
    return assignedStudentIds.length;
  },
  countUnassignedStudents: function(lab) {
    var studentIds = StudentRegistrations.find({courseId: lab.courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
    var assignedStudentIds = [];
    LabTeams.find({labId: lab._id, removed: {$ne: true}}).forEach(function(labTeam) {
      assignedStudentIds = assignedStudentIds.concat(labTeam.memberIds);
    });
    return studentIds.length - assignedStudentIds.length;
  },
});
