Template.labRubricPage.helpers({
  getQuestionItems: function(labId) {
		return ContentItems.find({containerId: labId, $or: [{kind: "question"}, {kind: "fileSubmission"}]}, {sort: {index: 1}});
	},
});
