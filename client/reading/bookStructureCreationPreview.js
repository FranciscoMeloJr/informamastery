Template.bookStructureCreationPreview.events({
		'click .create-structure-button': function(event, template) {
			console.log("click create-structure-button");
			console.log(this);

			var courseId = this.courseId;
			var bookId = this.bookId;
			var structure = this.structure;
			Meteor.call('bulkCreateBookStructure', bookId, structure, function(error, result) {
				if (error) {
					throwError(error.reason);
				} else {
					Router.go("bookPage", {courseId: courseId, bookId: bookId});
				}
			});
		},
});
