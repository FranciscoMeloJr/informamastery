Template.chapterEditPage.events({
	'submit .form': function(event, template) {
		var courseId = this.course._id;
		var bookId = this.book._id;
		var chapterId = this.chapter._id;
		var title = event.target.title.value;
		var number = event.target.number.value;
		var sortKey = event.target.sortKey.value;
		Meteor.call('updateChapter', chapterId, title, number, sortKey, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("chapterPage", {courseId: courseId, bookId: bookId, chapterId: chapterId});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .add-section-button': function(ev, template) {
		var courseId = this.course._id;
		var bookId = this.book._id;
		var chapterId = this.chapter._id;
		Meteor.call('addSection', chapterId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("sectionEditPage", {courseId: courseId, bookId: bookId, chapterId: chapterId, sectionId: result});
			}
		});
	},
	'click .remove-section-button': function(ev, template) {
		var sectionId = this._id;
		Meteor.call('removeSection', sectionId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
