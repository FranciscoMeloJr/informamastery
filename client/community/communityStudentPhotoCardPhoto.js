Template.communityStudentPhotoCardPhoto.rendered = function() {
  console.log("Template.communityStudentPhotoCardPhoto.rendered: called");
  $('.special.cards .dimmable.image').dimmer({
    on: 'hover',
  });
};

Template.communityStudentPhotoCardPhoto.helpers({
  getPhoto: function(studentRegistrationId) {
    console.log("Template.communityStudentPhotoCardPhoto.helpers:getPhoto("+studentRegistrationId+")");
		return StudentPhotoFiles.findOne({"metadata.studentRegistrationId": studentRegistrationId});
	},
  getPhotoLink: function() {
    return StudentPhotoFiles.baseURL + "/md5/" + this.md5;
  },
});

Template.communityStudentPhotoCardPhoto.events({
  'click .remove-photo-button': function(e, t) {
    console.log("communityStudentPhotoCardPhoto: click .remove-photo-button");
    var studentRegistration = t.data.studentRegistration;
    console.log("studentRegistration: ", studentRegistration);
    console.log("studentRegistrationId: ", studentRegistration._id);
    Meteor.call('removeStudentPhoto', studentRegistration._id);
  },
});
