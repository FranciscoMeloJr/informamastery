Template.communityStudentPhotoUploadArea.events({
  'click .uploadButton': function(e, t) {
    console.log("communityStudentPhotoUploadArea: click .uploadButton");
    e.preventDefault();
    e.stopPropagation();
    var input = t.find(".uploadInput");
    console.log("input:", input);
    input.click();
  },
  'change .uploadInput': function(e, t) {
    console.log("communityStudentPhotoUploadArea: change .uploadInput");
    attachFiles(e.target.files, e, t);
    e.target.value = '';
  },
  'dragenter .uploadArea': function(e) {
    console.log("communityStudentPhotoUploadArea: dragenter .uploadArea");
    e.preventDefault();
    e.stopPropagation();
  },
  'dragover .uploadArea': function(e) {
    console.log("communityStudentPhotoUploadArea: dragover .uploadArea");
    e.preventDefault();
    e.stopPropagation();
  },
  'drop .uploadArea': function(e, t) {
    console.log("communityStudentPhotoUploadArea: drop .uploadArea");
    e.preventDefault();
    e.stopPropagation();
    attachFiles(e.originalEvent.dataTransfer.files, e, t);
  },
});

function attachFiles(files, e, t) {
  console.log("attachFiles(", files, e, t, ")");
  var studentRegistration = t.data.studentRegistration;
  console.log("studentRegistration: ", studentRegistration);
  console.log("studentRegistrationId: ", studentRegistration._id);
  _.each(files, function(file) {
    /*
    //This callback does not seem to be called
    file.callback = function(file2) {
      console.log("upload: callback(", file2, ")");
    };
    */
    file.courseId = studentRegistration.courseId;
    file.userId = studentRegistration.studentId;
    file.studentRegistrationId = studentRegistration._id;
    StudentPhotoFiles.resumable.addFile(file, e);
    console.log("upload: addFile(", file, ", ", e,")");
  });
}
