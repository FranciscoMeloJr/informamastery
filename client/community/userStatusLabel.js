Template.userStatusLabel.rendered = function() {
	console.log("Template.userStatusLabel.rendered");
	this.$('.label').popup({
    inline: true
  });
};

Template.userStatusLabel.helpers({
	findInstructorRegistrationByCourseAndUser: function(courseId, userId) {
		return InstructorRegistrations.findOne({courseId: courseId, instructorId: userId});
	},
	findAssistantRegistrationByCourseAndUser: function(courseId, userId) {
		return AssistantRegistrations.findOne({courseId: courseId, assistantId: userId});
	},
	getGravatarUrl: function(user) {
		return Gravatar.imageUrl(user.emails[0].address, {size: 100, secure: window.location.protocol==="https:"});
	},
  getPhotoUrl: function(user) {
		var photo = StudentPhotoFiles.findOne({"metadata.userId": user._id});
		if (photo) {
			return StudentPhotoFiles.baseURL + "/md5/" + photo.md5;
		} else {
			return Gravatar.imageUrl(user.emails[0].address, {size: 100, secure: window.location.protocol==="https:"});
		}
  },
});
