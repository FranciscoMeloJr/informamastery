var createNextChallenge;

Template.communityGetToKnowStudentsPage.created = function () {
  console.log("Template.communityGetToKnowStudentsPage.created()");
  var self = this;
	// ReactiveVar to hold the bulk create student data
	this.challenge = new ReactiveVar();
  createNextChallenge = function() {
    console.log("createNextChallenge()");
    var dataContext = Template.currentData();
    console.log("dataContext:", dataContext);
    var courseId = dataContext._id;
    console.log("courseId:", courseId);
    var registeredUserIds = StudentRegistrations.find({courseId: courseId, approved: true, withdrawn: false}).map(function (reg) {return reg.studentId;});
    console.log("registeredUserIds:", registeredUserIds);
    var availableUserIds = StudentPhotoFiles.find({"metadata.userId": {$in: registeredUserIds}}).map(function (p) {return p.metadata.userId;});
    console.log("availableUserIds:", availableUserIds);
    var userIds = [];
    for (i=0; i<2; i++) {
      console.log("pick #"+i);
      var index = Math.floor(Math.random()*availableUserIds.length);
      var userId = availableUserIds[index];
      console.log(index+": "+userId);
      availableUserIds.splice(index, 1);
      userIds.push(userId);
    }
    var challenge = {
      pictureByName: Math.random()>=0.5,
      userIds: userIds,
      requestedUserId: userIds[Math.floor(Math.random()*userIds.length)],
    };
    console.log("challenge:", challenge);
    self.challenge.set(challenge);
  };
};

Template.communityGetToKnowStudentsPage.helpers({
  getStudentCount: function(courseId) {
		return StudentRegistrations.find({courseId: courseId, withdrawn: false}).count();
	},
  getChallenge: function() {
		console.log("Template.communityGetToKnowStudentsPage.helpers.getChallenge()");
		// retrieve the challenge from the ReactiveVar set below
		var challenge = Template.instance().challenge.get();
		console.log(challenge);
		return challenge;
	},
});

Template.communityGetToKnowStudentsPage.events({
  'click .start-button': function(event, template) {
    console.log(this);
		console.log("click .start-button");
    console.log(event);
    console.log(template);
    createNextChallenge();
  },
  'click .next-button': function(event, template) {
    console.log(this);
		console.log("next .start-button");
    console.log(event);
    console.log(template);
    createNextChallenge();
  },
  'click .left-button': function(event, template) {
    console.log(this);
		console.log("click .left-button");
    console.log(event);
    console.log(template);
    var challenge = Template.instance().challenge.get();
    challenge.guessed = true;
    if (challenge.userIds[0]==challenge.requestedUserId) {
      challenge.correct = true;
    } else {
      challenge.incorrect = true;
    }
    Meteor.call('updateCommunityGetToKnowResults', challenge.requestedUserId, challenge.pictureByName, challenge.correct, function(error, success) {
      if (error) {
        throwError(error.reason);
      }
    });
    // trigger reactive rendering
    Template.instance().challenge.set(challenge);
  },
  'click .right-button': function(event, template) {
    console.log(this);
		console.log("click .right-button");
    console.log(event);
    console.log(template);
    var challenge = Template.instance().challenge.get();
    challenge.guessed = true;
    if (challenge.userIds[1]==challenge.requestedUserId) {
      challenge.correct = true;
    } else {
      challenge.incorrect = true;
    }
    Meteor.call('updateCommunityGetToKnowResults', challenge.requestedUserId, challenge.pictureByName, challenge.correct, function(error, success) {
      if (error) {
        throwError(error.reason);
      }
    });
    // trigger reactive rendering
    Template.instance().challenge.set(challenge);
  },
});
