Template.userCourseRegistrationRow.helpers({
	getUserStudentRegistration: function(userId, courseId) {
		return StudentRegistrations.findOne({studentId: userId, courseId: courseId});
	},
	getUserAssistantRegistration: function(userId, courseId) {
		return AssistantRegistrations.findOne({assistantId: userId, courseId: courseId});
	},
	getUserInstructorRegistration: function(userId, courseId) {
		return InstructorRegistrations.findOne({instructorId: userId, courseId: courseId});
	},
});


Template.userCourseRegistrationRow.events({
	'click .register-as-student-button': function(ev, template) {
		console.log("register-as-student-button clicked");
	    var course = Template.currentData();
	    console.log("  Template.currentData() -- course: ");
	    console.log(course);
	    var student = Template.parentData(1); //TODO This does not always work, how to get the user?
	    console.log("  Template.parentData(1) -- student: ");
	    console.log(student);
		Meteor.call('registerUserAsStudent', student._id, course._id, function(error, studentRegistrationId) {
			console.log("  returned from call to registerUserAsStudent");
			if (error) {
				console.log("  with an error: ");
				console.log(error);
				throwError(error.reason);
			}
		});
	},
	'click .register-as-assistant-button': function(ev, template) {
		console.log("register-as-assistant-button clicked");
	    var course = Template.currentData();
	    console.log("  Template.currentData() -- course: ");
	    console.log(course);
	    var assistant = Template.parentData(1); //TODO This does not always work, how to get the user?
	    console.log("  Template.parentData(1) -- assistant: ");
	    console.log(assistant);
		Meteor.call('registerUserAsAssistant', assistant._id, course._id, function(error, assistantRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .register-as-instructor-button': function(ev, template) {
		console.log("register-as-instructor-button clicked");
	    var course = Template.currentData();
	    console.log("  Template.currentData() -- course: ");
	    console.log(course);
	    var instructor = Template.parentData(1); //TODO This does not always work, how to get the user?
	    console.log("  Template.parentData(1) -- instructor: ");
	    console.log(instructor);
		Meteor.call('registerUserAsInstructor', instructor._id, course._id, function(error, instructorRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
