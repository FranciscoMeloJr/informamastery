Template.userPage.helpers({
	getStudentRegistration: function(userId, courseId) {
    console.log("Template.userPage.getStudentRegistration("+userId+", "+courseId+")");
		var sr = StudentRegistrations.findOne({courseId: courseId, studentId: userId});
    console.log(sr);
    return sr;
	},
  getAssistantRegistration: function(userId, courseId) {
    console.log("Template.userPage.getAssistantRegistration("+userId+", "+courseId+")");
    var ar = AssistantRegistrations.findOne({courseId: courseId, assistantId: userId});
    console.log(ar);
    return ar;
  },
  getInstructorRegistration: function(userId, courseId) {
    console.log("Template.userPage.getInstructorRegistration("+userId+", "+courseId+")");
    var ir = InstructorRegistrations.findOne({courseId: courseId, instructorId: userId});
    console.log(ir);
    return ir;
  },
});
