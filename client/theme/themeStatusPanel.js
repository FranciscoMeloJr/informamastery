Template.themeStatusPanel.events({
  'click .show-button': function(event, template) {
		console.log("click .show-button");
		var themeId = this._id;
		Meteor.call("showTheme", themeId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .hide-button': function(event, template) {
		console.log("click .hide-button");
		var themeId = this._id;
		Meteor.call("hideTheme", themeId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
