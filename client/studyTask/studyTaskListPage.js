Template.studyTaskListPage.helpers({
  getTopicsForCourse: function(courseId) {
    //console.log("studyTaskListPage::getTopicsForCourse("+courseId+")");
    var ts = Topics.find({courseId: courseId, removed: {$ne: true}}, {sort: {masteryRequiredForGrade: 1}} );
    //console.log(ts.fetch());
    return ts;
  },
  getStudyTasksForTopic: function(topicId) {
    //console.log("studyTaskListPage::getStudyTasksForTopic("+topicId+")");
    var ss = StudyTasks.find({topicId: topicId, removed: {$ne: true}} );
    return ss;
  },
  countStudyTasksForTopic: function(topicId) {
    //console.log("studyTaskListPage::countStudyTasksForTopic("+topicId+")");
    var ssc = StudyTasks.find({topicId: topicId, removed: {$ne: true}} ).count();
    //console.log(ssc);
    return ssc;
  },
});
