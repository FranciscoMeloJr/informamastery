Template.studyTaskPage.helpers({
	getOwnSectionStatus: function(sectionId) {
		console.log("Template.studyTaskPage.helpers.getOwnSectionStatus("+sectionId+")");
		console.log(SectionStatus.findOne({sectionId: sectionId, userId: Meteor.userId()}));
		return SectionStatus.findOne({sectionId: sectionId, userId: Meteor.userId()});
	},
	getOwnStatusOrNullObject: function(studyTaskId) {
		console.log("Template.studyTaskPage.helpers.getOwnStatusOrNullObject("+studyTaskId+")");
		var status = StudyTaskStatus.findOne({studyTaskId: studyTaskId, userId: Meteor.userId()});
		if (!status) {
			status = {};
		}
		return status;
	},
	prepareState: function(status) {
		if (status.started || status.finished || status.recallText) {
			return "completed";
		} else {
			return "active";
		}
	},
	studyState: function(status) {
		if (status.started && !status.finished) {
			return "active";
		} else if (status.finished || status.recallText) {
			return "completed";
		} else {
			return "disabled";
		}
	},
	recallState: function(status) {
		if (status.finished && !status.recallText) {
			return "active";
		} else if (status.recallText) {
			return "completed";
		} else {
			return "disabled";
		}
	},
	reviewState: function(status) {
		return status.recallText?"active":"disabled";
	},
	stageIs: function(status, stage) {
		var s = "";
		if (status.recallText) {
			s="review";
		} else if (status.finished) {
			s="recall";
		} else if (status.started) {
			s="study";
		} else {
			s="prepare";
		}
		return stage==s;
	},
});



Template.studyTaskPage.events({
	'click .start-button': function(event, template) {
		console.log("Template.studyTaskPage click .start-button");
		console.log("this:", this);
		console.log("Template.parentData(0):", Template.parentData(0));
		console.log("Template.parentData(1):", Template.parentData(1));
		console.log("Template.parentData(2):", Template.parentData(2));
		var studyTask = Template.parentData(0).studyTask;
		var studyTaskId = studyTask._id;
		Meteor.call('startStudyTask', studyTaskId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .finish-button': function(event, template) {
		console.log("Template.studyTaskPage click .finish-button");
		console.log("this:", this);
		console.log("Template.parentData(0):", Template.parentData(0));
		console.log("Template.parentData(1):", Template.parentData(1));
		console.log("Template.parentData(2):", Template.parentData(2));
		var studyTask = Template.parentData(0).studyTask;
		var studyTaskId = studyTask._id;
		Meteor.call('finishStudyTask', studyTaskId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'submit .form': function(event, template) {
		event.preventDefault();
		console.log("Template.studyTaskPage click .save-recall-text-button");
		console.log("this:", this);
		console.log("Template.parentData(0):", Template.parentData(0));
		console.log("Template.parentData(1):", Template.parentData(1));
		console.log("Template.parentData(2):", Template.parentData(2));
		var studyTask = Template.parentData(0).studyTask;
		var studyTaskId = studyTask._id;
		var recallText = event.target.recallText.value;
		Meteor.call('updateStudyTaskRecallText', studyTaskId, recallText, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
