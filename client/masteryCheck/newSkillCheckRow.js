Template.newSkillCheckRow.events({
	'click .set-mastered-button': function(ev, template) {
		var skillCheckId = Template.currentData()._id;
		console.log(".set-mastered-button clicked: skillCheckId: "+skillCheckId);
		Meteor.call('setSkillCheckMastered', skillCheckId, true, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .clear-mastered-button': function(ev, template) {
		var skillCheckId = Template.currentData()._id;
		console.log(".clear-mastered-button clicked: skillCheckId: "+skillCheckId);
		Meteor.call('setSkillCheckMastered', skillCheckId, false, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
