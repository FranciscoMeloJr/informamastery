Template.newTopicCheckTablePart.helpers({
	getSkillChecksForTopicCheck: function(topicCheckId) {
		return SkillChecks.find({topicCheckId: topicCheckId});
	},
});

Template.newTopicCheckTablePart.events({
	'blur .topic-feedback': function(ev, template) {
		var topicCheckId = Template.currentData()._id;
		console.log(".topic-feedback area blurred: topicCheckId: "+topicCheckId);
	  var feedback = ev.target.value;
		Meteor.call('setTopicCheckFeedback', topicCheckId, feedback, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
