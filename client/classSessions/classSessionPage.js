Template.classSessionPage.helpers({
	getTopics: function(topicIds) {
    if (!topicIds) {
      topicIds = [];
    }
		return Topics.find({_id: {$in: topicIds}}, {sort: {masteryRequiredForGrade: 1}});
	},
});
