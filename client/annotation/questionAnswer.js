Template.questionAnswer.created = function () {
	// ReactiveVar to hold the editing state
	this.isEditing = new ReactiveVar(false);
};


Template.questionAnswer.rendered = function(){
	this.$('.initially-focused').focus();
};


Template.questionAnswer.helpers({
	isEditing: function() {
		// retrieve the isEditing state from the ReactiveVar set below
		return Template.instance().isEditing.get();
	},
	annotationHasOtherAcceptedAnswer: function(annotationId, answerId) {
		return AnnotationResponses.find({annotationId: annotationId, _id: {$ne: answerId}, accepted: true}).count()>0;
	},
	annotationIsMine: function(annotationId) {
		console.log("Template.questionAnswer.helpers.annotationIsMine("+annotationId+")")
		var result = Annotations.find({_id: annotationId, userId: Meteor.userId()}).count()>0;
		console.log("--> "+result);
		return result;
	},
	isOwnAnswer: function(answer) {
		return answer?(answer.userId===Meteor.userId()):undefined;
	},
	haveUpvoted: function(answer) {
		return answer.upvoteUserIds && _.contains(answer.upvoteUserIds, Meteor.userId());
	},
	haveDownvoted: function(answer) {
		return answer.downvoteUserIds && _.contains(answer.downvoteUserIds, Meteor.userId());
	},
	haveNotVoted: function(answer) {
		return ( !answer.upvoteUserIds || !_.contains(answer.upvoteUserIds, Meteor.userId()) )
		&& ( !answer.downvoteUserIds || !_.contains(answer.downvoteUserIds, Meteor.userId()) );
	},
});


Template.questionAnswer.events({
	'click .accept-answer-button': function(event, template) {
		console.log("Template.questionAnswer.events click .accept-answer-button");
		var answerId = this._id;
		Meteor.call("acceptAnswer", answerId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .upvote-answer-button': function(event, template) {
		console.log("Template.questionAnswer.events click .upvote-answer-button");
		var answerId = this._id;
		Meteor.call("upvoteAnswer", answerId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .downvote-answer-button': function(event, template) {
		console.log("Template.questionAnswer.events click .downvote-answer-button");
		var answerId = this._id;
		Meteor.call("downvoteAnswer", answerId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .clearvote-answer-button': function(event, template) {
		console.log("Template.questionAnswer.events click .clearvote-answer-button");
		var answerId = this._id;
		Meteor.call("clearvoteAnswer", answerId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .remove-answer-button': function(event, template) {
		console.log("Template.questionAnswer.events click .remove-answer-button");
		var answerId = this._id;
		Meteor.call("removeAnswer", answerId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .edit-answer-button': function(event, template) {
		console.log("click .edit-answer-button");
		var isEditing = template.isEditing.get();
		if (isEditing) {
			// save modified answer text
			var annotationResponseId = this._id;
			console.log(annotationResponseId);
			var text = $('.answer-text-field').val();
			console.log(text);
			Meteor.call("updateAnswerText", annotationResponseId, text, function(error, result) {
				if (error) {
					console.log("Error");
					throwError(error.reason);
				} else {
					console.log("removed.");
				}
			});
		}
		// store the isEditing state in the ReactiveVar retrieved above
		template.isEditing.set(!isEditing);
	},
});
