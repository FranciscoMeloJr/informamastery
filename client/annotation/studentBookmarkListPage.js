Template.studentBookmarkListPage.helpers({
	countUserBookmarksForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, kind: "bookmark", removed: false}).count();
	},
	getUserBookmarksForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, kind: "bookmark", removed: false}, {sort: {modificationDate: -1}});
	},
	containerIs: function(itemId, collectionName) {
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
});


Template.studentBookmarkListPage.events({
	'click .remove-annotation-button': function(ev, template) {
		var annotationId = this._id;
		//console.log(".remove-annotation-button clicked");
		Meteor.call('removeAnnotation', annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
