Template.studentQuestionListPage1.helpers({
	countUserQuestionsForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, kind: "question", removed: false}).count();
	},
	getUserQuestionsForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, kind: "question", removed: false}, {sort: {modificationDate: -1}});
	},
	containerIs: function(itemId, collectionName) {
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
});


Template.studentQuestionListPage1.events({
	'click .remove-annotation-button': function(ev, template) {
		var annotationId = this._id;
		//console.log(".remove-annotation-button clicked");
		Meteor.call('removeAnnotation', annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
