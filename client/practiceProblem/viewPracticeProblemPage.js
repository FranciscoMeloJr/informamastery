_mySolution = function(practiceProblemId) {
	return PracticeProblemSolutions.findOne({practiceProblemId: practiceProblemId, userId: Meteor.userId()});
}


Template.viewPracticeProblemPage.created = function() {
	console.log("Template.viewPracticeProblemPage.created");
	var pageCreated = moment(new Date());
	// We theoretically could show the timer on the page
	// (but it might be distracting)
	this.timer = Meteor.setInterval(function() {
		var now = moment(new Date());
		var seconds = now.diff(pageCreated, 'seconds');
		Session.set("secondsSincePageCreated", seconds);
	}, 1000);
};


Template.viewPracticeProblemPage.destroyed = function() {
	console.log("Template.viewPracticeProblemPage.destroyed");
	Meteor.clearInterval(this.timer);
};


Template.viewPracticeProblemPage.helpers({
	activeIfTrue: function(v) {
		return v?"active":"";
	},
	isFieldTrue: function(object, fieldName) {
		return object[fieldName];
	},
	activeIfFieldTrue: function(object, fieldName) {
		console.log("activeIfContains("+object+", "+fieldName+")");
		console.log(object);
		return object[fieldName]?"active":"";
	},
	isMine: function(userId) {
		return userId===Meteor.userId();
	},
	mySolution: function(practiceProblemId) {
		return _mySolution(practiceProblemId);
	},
	myProblemOrMySolution: function(practiceProblem) {
		if (practiceProblem) {
			return practiceProblem.userId===Meteor.userId() || _mySolution(practiceProblem._id);
		} else {
			return false;
		}
	},
	isMineOrHaveSolved: function(userId, practiceProblemId) {
		return userId===Meteor.userId()
			|| PracticeProblemSolutions.findOne({practiceProblemId: practiceProblemId, userId: Meteor.userId()});
	},
	allowedToSolve: function(userId, practiceProblemId) {
		return userId!==Meteor.userId()
			&& !PracticeProblemSolutions.findOne({practiceProblemId: practiceProblemId, userId: Meteor.userId()});
	},
	getSolutions: function(practiceProblemId) {
		return PracticeProblemSolutions.find({practiceProblemId: practiceProblemId});
	},
	getArrayLength: function(array) {
		return array?array.length:"?";
	},
	containsMe: function(array) {
		return _.contains(array, Meteor.userId());
	},
	isMe: function(userId) {
		return userId===Meteor.userId();
	},
});


Template.viewPracticeProblemPage.events({
	'click .archive-practice-problem-button': function (event) {
		var practiceProblemId = this.practiceProblem._id;
		Meteor.call('archivePracticeProblem', practiceProblemId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .publish-practice-problem-button': function (event) {
		var practiceProblemId = this.practiceProblem._id;

		//TODO: show modal dialog prompting users:
		// “Every job you do has your signature on it — do you really want to sign that?”
		// - Stuart Butterfield (founder of Flickr and Slack)
		//   https://medium.com/@stewart/rules-of-business-e71353753e11
		// Or maybe don't show, because it's an extrinsic motivator:
		// "People are often moved by external factors such as
		//  reward systems, grades, evaluations,
		//  **or the opinions they fear others might have of them.**"
		//  -- http://www.selfdeterminationtheory.org/theory/
		Meteor.call('publishPracticeProblem', practiceProblemId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .clone-practice-problem-button': function (event) {
		var practiceProblem = this.practiceProblem;
		var practiceProblemId = this.practiceProblem._id;
		Meteor.call('clonePracticeProblem', practiceProblemId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				if (!practiceProblem.archived) {
					Meteor.call('archivePracticeProblem', practiceProblemId, function(error, result) {
						if (error) {
							throwError(error.reason);
						}
					});
				}
				Router.go("editPracticeProblemPage", {courseId: practiceProblem.courseId, topicId: practiceProblem.topicId, practiceProblemId: result});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .feature-practice-problem-button': function(event) {
		console.log("click .feature-practice-problem-button");
		event.preventDefault();
		var practiceProblem = this.practiceProblem;
		var practiceProblemId = this.practiceProblem._id;
		Meteor.call('featurePracticeProblem', practiceProblemId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .unfeature-practice-problem-button': function(event) {
		console.log("click .unfeature-practice-problem-button");
		event.preventDefault();
		var practiceProblem = this.practiceProblem;
		var practiceProblemId = this.practiceProblem._id;
		Meteor.call('unfeaturePracticeProblem', practiceProblemId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .choice-toggle-button': function (event) {
		console.log(event);
		console.log(event.currentTarget);
		if ($(event.currentTarget).hasClass("active")) {
			$(event.currentTarget).removeClass("active");
			$(event.currentTarget).html("<i class='minus icon'></i> Wrong");
		} else {
			$(event.currentTarget).addClass("active");
			$(event.currentTarget).html("<i class='checkmark icon'></i> Correct");
		}
	},
	'click .submit-practice-problem-solution-button': function (event) {
		var practiceProblemId = this.practiceProblem._id;
		var selectedChoices = {};


		console.log("finding DOM nodes with class choice-container...");
		var choices = $(".choice-container").each(function(index, element) {
		//var choices = $(event.target).find(".choice-container").each(function(index, element) {
			console.log("found a DOM node with class choice-container");
			console.log(element);
			var label = $(element).find(".choice-toggle-button").attr("name");
			var isCorrect = $(element).find(".choice-toggle-button").hasClass("active");
			selectedChoices[label] = isCorrect;
		});
		console.log(selectedChoices);

		var duration = Session.get("secondsSincePageCreated");

		Meteor.call('submitPracticeProblemSolution', practiceProblemId, selectedChoices, duration, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .quality-button': function(event) {
		$(".quality-button").removeClass("active");
		$(event.target).addClass("active");
	},
	'click .difficulty-button': function(event) {
		$(".difficulty-button").removeClass("active");
		$(event.target).addClass("active");
	},
	'click .submit-practice-problem-feedback-button': function (event) {
		//var practiceProblemId = this.practiceProblem._id;
		//console.log("practiceProblemId: "+practiceProblemId);
		//var practiceProblemSolutionId = _mySolution(practiceProblemId)._id;
		var practiceProblemSolutionId = this._id;
		console.log("practiceProblemSolutionId: "+practiceProblemSolutionId);
		var difficulty = $(".difficulty-button.active").attr("name");
		var quality = $(".quality-button.active").attr("name");
		var comment = $(".comment-field").val();

		console.log("difficulty: "+difficulty);
		console.log("quality: "+quality);
		console.log("comment: "+comment);

		Meteor.call('submitPracticeProblemFeedback', practiceProblemSolutionId, difficulty, quality, comment, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .agree-with-comment-link': function(event) {
		var practiceProblemSolutionId = this._id;
		Meteor.call('agreeWithPracticeProblemComment', practiceProblemSolutionId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .disagree-with-comment-link': function(event) {
		var practiceProblemSolutionId = this._id;
		Meteor.call('disagreeWithPracticeProblemComment', practiceProblemSolutionId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
