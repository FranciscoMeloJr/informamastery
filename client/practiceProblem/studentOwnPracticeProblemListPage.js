_findStudentOwnPracticeProblems = function(userId) {
	return PracticeProblems.find({userId: userId});
}


Template.studentOwnPracticeProblemListPage.helpers({
	findStudentOwnPracticeProblems: function(userId) {
		return _findStudentOwnPracticeProblems(userId);
	},
	countStudentOwnPracticeProblems: function(userId) {
		return _findStudentOwnPracticeProblems(userId).count();
	},
	countStudentOwnPublishedUnarchivedPracticeProblems: function(studentId) {
		return PracticeProblems.find({userId: studentId, published: true, archived: false}).count();
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "createdDate",
					label: "Created",
					sort: "descending",
					tmpl: Template.practiceProblemCreatedDate,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "topic",
					label: "Topic",
					cellClass: "left aligned",
					fn: function(value, object) {
						if (object) {
							var topic = Topics.findOne({_id: object.topicId});
							return topic?topic.title:"";
						} else {
							return "";
						}
					},
					tmpl: Template.practiceProblemTopic,
				},
				{
					key: "title",
					label: "Title",
					cellClass: "left aligned",
					tmpl: Template.clickablePracticeProblemTitle,
				},
				{
					key: "solutions",
					label: "Solutions",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _countPracticeProblemSolutions(object._id)},
					tmpl: Template.practiceProblemSolutionCount,
				},
				{
					key: "difficulty",
					label: "Difficulty",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _computeAverageDifficultyRating(object._id)},
					tmpl: Template.practiceProblemDifficultyRating,
				},
				{
					key: "quality",
					label: "Quality",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _computeAverageQualityRating(object._id)},
					tmpl: Template.practiceProblemQualityRating,
				},
				{
					key: "EDIT",
					label: "Edit",
					sortable: false,
					tmpl: Template.practiceProblemEditButton,
					headerClass: "collapsing",
					cellClass: "center algined",
				},
				{
					key: "published",
					label: "P",
					tmpl: Template.practiceProblemPublished,
					headerClass: "center aligned collapsing",
					cellClass: "center aligned",
				},
				{
					key: "archived",
					label: "A",
					tmpl: Template.practiceProblemArchived,
					headerClass: "center aligned collapsing",
					cellClass: "center aligned",
				},
			],
		};
	},
});
