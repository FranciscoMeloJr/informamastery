Template.topicStudentMatrixPage.helpers({
	getTopicsForCourse: function(courseId) {
		return Topics.find({courseId: courseId}, {sort: {masteryRequiredForGrade: 1}} );
	},
	getStudentRegistrationsForCourse: function(courseId) {
		console.log("Template.topicStudentMatrixPage.helpers.getStudentRegistrationsForCourse("+courseId+")");
		return StudentRegistrations.find({courseId: courseId}, {sort: {creationDate: 1}});
	},
	getTopicStatusForTopicAndStudent: function(topicId, studentId) {
		return TopicStatus.findOne({topicId: topicId, studentId: studentId});
	},
});
