This is my hacky way to add version 0.9.6 of joint.js
and its dependencies to Informa.

The available Atmosphere packages contain older versions of joint.js
(0.9.3 or older).

Version 0.9.3 contains a bug that makes creating links
by dragging from a port unreliable
(sometimes one cannot connect the link to some other port).

The bug becomes visible in calls to validateConnection:
function(cellViewS, magnetS, cellViewT, magnetT, end, linkView)

BUG: Sometimes magnetS is the type==="input" of the element!
     How come, when we drag from an output port,
     that we get a call to validateConnection telling us it's from an input port???
FIX: issue 120: https://github.com/clientIO/joint/pull/120
     joint.dia.cell.js, function getSelector
     existed in v0.9.3
     fixed in
     https://github.com/clientIO/joint/commit/96ff19fe9c781a534ca487476206f2e479b8a55d
     May 22, 2015
     (so v0.9.4, from July 23, 2015, should include this fix)
