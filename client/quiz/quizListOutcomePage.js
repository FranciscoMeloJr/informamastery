Template.quizListOutcomePage.helpers({
  getQuizScoreSummary: function(courseId) {
    console.log("getQuizScoreSummary(", courseId, ")");
    var quizScoreSummary = Summaries.findOne({courseId: courseId, kind: "quizScoreSummary"} , {sort: {date: -1}, limit: 1});
    console.log("quizScoreSummary:", quizScoreSummary);
    return quizScoreSummary;
  },
  extractMaxScore: function(quizScoreSummary, quizId) {
    //console.log("extractMaxScore(", quizScoreSummary, quizId, ")");
    var maxScore = quizScoreSummary.data[quizId]["maxScore"];
    //console.log(maxScore);
    return maxScore;
  },
  extractScore: function(quizScoreSummary, quizId, userId) {
    //console.log("extractScore(", quizScoreSummary, quizId, userId, ")");
    var score = quizScoreSummary.data[quizId]["userScores"][userId];
    //console.log(score);
    return score;
  },
  getPastNonHiddenQuizzes: function(courseId) {
    return getPastNonHiddenQuizzes(courseId);
  },
  getApprovedNotWithdrawnUsers: function(courseId) {
    var studentIds = StudentRegistrations.find({courseId: courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
    console.log("studentIds:", studentIds);
    return Meteor.users.find({_id: {$in: studentIds}}, {sort: {"profile.firstName": 1}});
  },
  computeOverallScore: function(quizId, userId) {
    return computeOverallQuizScore(quizId, userId);
  },
  computeMaxOverallScore: function(quizId) {
    return computeMaxOverallQuizScore(quizId);
  },
  getQuizAttempt: function(quizId, studentId) {
    console.log("getQuizAttempt("+quizId+", "+studentId+")");
    // the LAST attempt of that quiz!
    var quiz = Quizzes.findOne({_id: quizId});
    console.log("quiz: ", quiz);
    var quizAttempt = QuizAttempts.findOne({quizId: quizId, studentId: studentId, quizStartDate: quiz.startDate});
    console.log("quizAttempt: ", quizAttempt);
    return quizAttempt;
  },
});
