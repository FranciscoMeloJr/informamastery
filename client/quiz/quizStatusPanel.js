Template.quizStatusPanel.helpers({
	getQuizAttemptCounts: function(quizId, quizStartDate) {
		return QuizAttempts.find({quizId: quizId, quizStartDate: quizStartDate}).count();
	},
  getMyQuizAttempt: function(quizId) {
    // the LAST attempt of that quiz!
    var quiz = Quizzes.findOne({_id: quizId});
    return QuizAttempts.findOne({quizId: quizId, studentId: Meteor.userId(), quizStartDate: quiz.startDate});
  },
});


Template.quizStatusPanel.events({
  'click .show-button': function(event, template) {
		console.log("click .show-button");
		var quizId = this.quiz._id;
		Meteor.call("showQuiz", quizId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .hide-button': function(event, template) {
		console.log("click .hide-button");
		var quizId = this.quiz._id;
		Meteor.call("hideQuiz", quizId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .start-quiz-button': function(ev, template) {
		var courseId = this.course._id;
		var quizId = this.quiz._id;
		Meteor.call('startQuiz', quizId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("quizRunPage", {courseId: courseId, quizId: quizId});
			}
		});
	},
});
