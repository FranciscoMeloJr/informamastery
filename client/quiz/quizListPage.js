function _getTodaysQuizzes(courseId) {
  var start = moment().startOf('day').toDate();
  var end = moment().endOf('day').toDate();
  console.log("start: ", start);
  console.log("end: ", end);
  return Quizzes.find({courseId: courseId, removed: false, date: {$gte: start, $lte: end}}, {sort: {date: 1}});
}

function _getPastQuizzes(courseId) {
  var start = moment().startOf('day').toDate();
  return Quizzes.find({courseId: courseId, removed: false, date: {$lt: start}}, {sort: {date: 1}});
}

function _getFutureQuizzes(courseId) {
  var end = moment().endOf('day').toDate();
  return Quizzes.find({courseId: courseId, removed: false, date: {$gt: end}}, {sort: {date: 1}});
}


Template.quizListPage.helpers({
  getTodaysQuizzes: function(courseId) {
    return _getTodaysQuizzes(courseId);
  },
  countTodaysQuizzes: function(courseId) {
    return _getTodaysQuizzes(courseId).count();
  },
  getPastQuizzes: function(courseId) {
    return _getPastQuizzes(courseId);
  },
  countPastQuizzes: function(courseId) {
    return _getPastQuizzes(courseId).count();
  },
  getFutureQuizzes: function(courseId) {
    return _getFutureQuizzes(courseId);
  },
  countFutureQuizzes: function(courseId) {
    return _getFutureQuizzes(courseId).count();
  },
});
