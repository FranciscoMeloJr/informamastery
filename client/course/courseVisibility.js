Template.courseVisibility.events({
	'click .show-course-button': function (event) {
		var courseId = this._id;
		Meteor.call('showCourse', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .hide-course-button': function (event) {
		var courseId = this._id;
		Meteor.call('hideCourse', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
});
