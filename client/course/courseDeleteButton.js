Template.courseDeleteButton.helpers({
	canDelete: function(courseId) {
		var i = InstructorRegistrations.find({courseId: courseId}).count();
		var a = AssistantRegistrations.find({courseId: courseId}).count();
		var s = StudentRegistrations.find({courseId: courseId}).count();
		return (i+a+s)==0;
	},
});


Template.courseDeleteButton.events({
	'click .delete-course-button': function (event) {
		var courseId = this._id;
		Meteor.call('deleteCourse', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
});
