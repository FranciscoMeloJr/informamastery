Template.courseMenu.helpers({
	isProfileComplete: function() {
		var user = Meteor.user();
		return user.profile
			&& user.profile.firstName
			&& user.profile.firstName!=""
			&& user.profile.lastName
			&& user.profile.lastName!=""
			&& user.profile.biography
			&& user.profile.biography!="";
	},
	getMyUserId() {
		return Meteor.user()._id;
	},
  getStudentRegistrationForCourse: function(courseId) {
		return StudentRegistrations.findOne({courseId: courseId, studentId: Meteor.user()._id});
	},
  getAssistantRegistrationForCourse: function(courseId) {
		return AssistantRegistrations.findOne({courseId: courseId, assistantId: Meteor.user()._id});
	},
  getInstructorRegistrationForCourse: function(courseId) {
		return InstructorRegistrations.findOne({courseId: courseId, instructorId: Meteor.user()._id});
	},
	getOngoingQuiz: function(courseId) {
		return Quizzes.findOne({courseId: courseId, ongoing: true});
	}
});


Template.courseMenu.events({
	'click button': function(ev, template) {
		var courseId = Session.get("course")._id;
		console.log("button clicked: self-registering for course "+courseId);
		Meteor.call('selfRegisterAsStudent', courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
