Template.adminCourseListPage.helpers({
	findCourses: function() {
		return Courses.find({deleted: {$ne: true}});
	},
	settings: {
		class: "ui compact table",
		fields: [
			{
				key: "abbreviation",
				label: "Abbreviation",
				sort: "ascending",
				headerClass: "collapsing",
			},
			{
				key: "instance",
				label: "Instance",
				headerClass: "collapsing",
			},
			{
				key: "visible",
				label: "Visible",
				headerClass: "collapsing",
				cellClass: "center aligned",
				tmpl: Template.courseVisibility,
			},
			{
				key: "openForRegistration",
				label: "Open for Registration",
				headerClass: "collapsing",
				cellClass: "center aligned",
				tmpl: Template.courseOpenForRegistration,
			},
			{
				key: "title",
				label: "Title",
				tmpl: Template.clickableCourseTitle,
			},
			{
				key: "instructors",
				label: "#I",
				headerClass: "collapsing",
				cellClass: "right aligned",
				fn: function(value, object) {return InstructorRegistrations.find({courseId: object._id}).count();},
			},
			{
				key: "assistants",
				label: "#A",
				headerClass: "collapsing",
				cellClass: "right aligned",
				fn: function(value, object) {return AssistantRegistrations.find({courseId: object._id}).count();},
			},
			{
				key: "students",
				label: "#S",
				headerClass: "collapsing",
				cellClass: "right aligned",
				fn: function(value, object) {return StudentRegistrations.find({courseId: object._id}).count();},
			},
			{
				key: "clone",
				label: "Clone",
				headerClass: "collapsing",
				cellClass: "right aligned",
				tmpl: Template.courseCloneButton,
			},
			{
				key: "delete",
				label: "Delete",
				headerClass: "collapsing",
				cellClass: "right aligned",
				tmpl: Template.courseDeleteButton,
			},
		],
	},
});


Template.adminCourseListPage.events({
	'submit .form': function (event) {
		var abbreviation = event.target.abbreviation.value;
		var instance = event.target.instance.value;
		var title = event.target.title.value;
		Meteor.call('createCourse', abbreviation, instance, title, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("coursePage", {courseId: result});
			}
		});
		// Prevent default form submit
		return false;
	},
});
