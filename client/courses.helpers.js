Template.courseSidebar.rendered = function(){
  this.$('.ui.button').popup();
  this.$('.ui.dropdown').dropdown();
};


Template.topicTable.helpers({
	lookupThemeTitle: function(themeId) {
		return Themes.findOne({_id: themeId}).title;
	},
	lookupThemeIcon: function(themeId) {
		return Themes.findOne({_id: themeId}).icon;
	},
	lookupThemeColor: function(themeId) {
		return Themes.findOne({_id: themeId}).color;
	},
});
