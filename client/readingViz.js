var SectionStatusObserver;
var QuizzesObserver;


Template.readingViz.onRendered(function() {
  console.log("Template.readingViz.onRendered:");
  console.log("this.$('#readingViz'):", this.$('.readingViz'));

  console.log("1. Template.currentData():", Template.currentData());
  var courseId = Template.currentData().courseId;
  console.log("2. courseId:", courseId);
  var userId = Template.currentData().studentId;
  console.log("3. userId:", userId);

  //Width and height
  var w = 600;
  var h = 200;
  var padding = 40;
  var padTop = 5;
  var padBottom = 40;
  var padLeft = 40;
  var padRight = 5;

  // Create scale functions
  var xScale = d3.time.scale()
    .range([padLeft, w-padRight]);

  var yScale = d3.scale.ordinal()
    .rangePoints([h-padBottom, padTop]);

  // Define X axis
  var xAxis = d3.svg.axis()
    .scale(xScale)
    .orient("bottom")
    .ticks(d3.time.week, 1);

  // Define Y axis
  var yAxis = d3.svg.axis()
    .scale(yScale)
    .orient("left")
    .tickValues([]); // no ticks

  // Create SVG element
  var svg = d3.select("#readingViz")
    .attr("width", w)
    .attr("height", h);

  var courseStartDate = Courses.findOne(courseId).startDate;
  console.log("courseStartDate:", courseStartDate);
  var courseEndDate = Courses.findOne(courseId).endDate;
  console.log("courseEndDate:", courseEndDate);

  // Create course
  var course = svg.append("g")
    .attr("class", "course");
  course.append("rect")
    .attr("y", padTop)
    .attr("height", h-padBottom-padTop)
    .attr("id", "durationRect");
  course.append("line")
    .attr("y1", padTop)
    .attr("y2", h-padBottom)
    .attr("id", "startDateLine");
  course.append("line")
    .attr("y1", padTop)
    .attr("y2", h-padBottom)
    .attr("id", "endDateLine");

  // Create quizzes
  var quizzes = svg.append("g")
    .attr("class", "quizzes");

  // Create X axis
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + (h-padBottom) + ")");

  // now rotate text on x axis
  // solution based on idea here: https://groups.google.com/forum/?fromgroups#!topic/d3-js/heOBPQF3sAY
  // first move the text left so no longer centered on the tick
  // then rotate up to get 45 degrees.
  //svg.selectAll(".x.axis text")  // select all the text elements for the xaxis
  //  .attr("transform", function(d) {
  //    return "translate(" + this.getBBox().height*-2 + "," + this.getBBox().height + ")rotate(-45)";
  //  });

  //Create Y axis
  svg.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate(" + padLeft + ",0)");


  // now add titles to the axes
  svg.append("text")
    .attr("text-anchor", "middle")
    .attr("transform", "translate("+(padLeft/2)+","+(padTop+(h-padBottom-padTop)/2)+")rotate(-90)")
    .text("Section");

  svg.append("text")
    .attr("text-anchor", "middle")
    .attr("transform", "translate("+(padLeft+(w-padLeft-padRight)/2)+","+h+")")
    .text("Date Recalled");


  // Determine y scale domain
  var sectionNumbers = Sections.find({courseId: courseId}, {fields: {sortKey: 1, number: 1}, sort: {sortKey: 1}}).map(function(s) {return s.number;});
  console.log("sectionNumbers:", sectionNumbers);
  yScale.domain(sectionNumbers);

  //declare a transistion function to animate position and scale changes
  var transition = function() {
    //Update x scale domain
    var minDateStatus = _.first(SectionStatus.find({courseId: courseId, userId: userId}, {fields:{modificationDate: 1}, sort:{modificationDate:  1}, limit: 1}).fetch());
    var maxDateStatus = _.first(SectionStatus.find({courseId: courseId, userId: userId}, {fields:{modificationDate: 1}, sort:{modificationDate: -1}, limit: 1}).fetch());
    var minDate = minDateStatus&&minDateStatus.modificationDate<courseStartDate ? minDateStatus.modificationDate : courseStartDate;
    var maxDate = maxDateStatus&&maxDateStatus.modificationDate>courseEndDate ? maxDateStatus.modificationDate : courseEndDate;
    xScale.domain([minDate, maxDate]);

    //Update X axis
    svg.select(".x.axis")
      .transition()
      .duration(1000)
      .call(xAxis);

    //Update Y axis
    svg.select(".y.axis")
      .transition()
      .duration(1000)
      .call(yAxis);

    course.select("#durationRect")
      .transition()
      .duration(1000)
      .attr("x", xScale(courseStartDate))
      .attr("width", xScale(courseEndDate)-xScale(courseStartDate));
    course.select("#startDateLine")
      .transition()
      .duration(1000)
      .attr("x1", xScale(courseStartDate))
      .attr("x2", xScale(courseStartDate));
    course.select("#endDateLine")
        .transition()
        .duration(1000)
        .attr("x1", xScale(courseEndDate))
        .attr("x2", xScale(courseEndDate));

    svg
      .selectAll("circle")
      .transition()
      .duration(1000)
      .attr("cx", function(d) {
        return xScale(d.modificationDate);
      })
      .attr("cy", function(d) {
        //console.log("SectionStatus:", d);
        var section = Sections.findOne({_id: d.sectionId});
        //console.log("Section:", section);
        return yScale(section.number);
      });

    quizzes
      .selectAll("line")
      .transition()
      .duration(1000)
      .attr("x1", function(d) {
        return xScale(d.date);
      })
      .attr("x2", function(d) {
        return xScale(d.date);
      });
  };

  //transistion once right away to set the axis scale
  transition();

  //decalare a debounced version, only runs once per batch of calls, 50ms after last call - if a bunch of update occur at the same time, when only want to transisition once
  var lazyTransition = _.debounce(transition, 50);

  SectionStatusObserver = SectionStatus.find({courseId: courseId, userId: userId}, {fields: {modificationDate: 1, sectionId: 1}}).observe({
    added: function (document) {
      svg
        .append("circle")
        .datum(document) //attach data
        .attr("cx", function(d) { //TODO: remove? //set the points right away, even if scale is wrong
          return xScale(d.modificationDate);
        })
        .attr("cy", function(d) { //TODO: remove?
          console.log("SectionStatus:", d);
          var section = Sections.findOne({_id: d.sectionId});
          console.log("Section:", section);
          return yScale(section.number);
        })
        .attr("r", 2)
        .attr("id", "point-"+document._id); //set an id - FYI d3 does not like ids that start with digits
      lazyTransition(); //transition
    },
    changed: function (newDocument, oldDocument) {
      svg
        .select("#point-"+newDocument._id)
        .datum(newDocument); //update data
      lazyTransition(); //transition
    },
    removed: function (oldDocument) {
      svg
        .select("#point-"+oldDocument._id)
        .remove(); //remove element
      lazyTransition(); //transition
    }
  });


  QuizzesObserver = Quizzes.find({courseId: courseId}, {fields: {date: 1}}).observe({
    added: function (document) {
      quizzes
        .append("line")
        .datum(document) //attach data
        .attr("x1", function(d) {
          return xScale(d.date);
        })
        .attr("x2", function(d) {
          return xScale(d.date);
        })
        .attr("y1", padTop) //TODO: remove?
        .attr("y2", h-padBottom) //TODO: remove?
        .attr("id", "quiz-line-"+document._id); //set an id - FYI d3 does not like ids that start with digits
      lazyTransition(); //transition
    },
    changed: function (newDocument, oldDocument) {
      quizzes
        .select("#quiz-line-"+newDocument._id)
        .datum(newDocument); //update data
      lazyTransition(); //transition
    },
    removed: function (oldDocument) {
      quizzes
        .select("#quiz-line-"+oldDocument._id)
        .remove(); //remove element
      lazyTransition(); //transition
    }
  });

});


Template.readingViz.onDestroyed(function() {
  console.log("Template.readingViz.onDestroyed:");
  if (SectionStatusObserver) {
    SectionStatusObserver.stop(); //stop the query when the template is destroyed
  }
  if (QuizzesObserver) {
    QuizzesObserver.stop();
  }
});
