var testRoutes = function(routeNames) {
	//console.log("testRoutes("+routeNames+")");
	//console.log(Router.current());
	//if (Router.current() && Router.current().route) {
		//console.log(Router.current().route.getName());
	//}
	var regex = new RegExp(routeNames, 'i');
	return Router.current() && Router.current().route && regex.test(Router.current().route.getName());
};


Template.registerHelper('isActiveRoute', function(routes, className) {
	//console.log("isActiveRoute("+routes+", "+className+")");
	if (className.hash) {
		className = 'active';
	}
	return testRoutes(routes) ? className : '';
});
