Template.registerHelper("iconForStudyTaskKind", function(kind) {
	if (kind==="Textbook") {
		return "book";
	} else if (kind==="Video") {
		return "film";
	} else if (kind==="Exercise") {
		return "write";
	} else if (kind==="Paper") {
		return "file outline";
	} else if (kind==="Web") {
		return "cloud";
	} else if (kind==="Explanation") {
		return "info circle";
	} else if (kind=="ClassSession") {
		return "student";
	}
	return "";
});
