if (Meteor.isClient) {

  Template.registerHelper("getUserStatusInfo", function() {
    return "Monitoring: "+UserStatus.isMonitoring()+", Idle: "+UserStatus.isIdle();
  });


  // given that we use useraccounts:... and not the standard meteor accounts, this helper seems not to be defined
  // so let's define it here:
  Template.registerHelper("currentUser", function() {
    //console.log("currentUser helper called!");
    //console.log(Meteor.user());
    return Meteor.user();
  });

  Template.registerHelper("impersonating", function() {
    return Session.get("impersonating");
  });

  Template.registerHelper("currentUserEmail", function() {
    console.log("currentUserEmail helper called!");
    console.log(Meteor.user().emails[0]);
    return Meteor.user().emails[0].address;
  });

  Template.registerHelper("gravatarUrl", function() {
    if (Meteor.user()) {
      var email = Meteor.user().emails[0].address;
      return Gravatar.imageUrl(email, {size: 100, secure: window.location.protocol==="https:"});
    } else {
      return "";
    }
  });

  Template.registerHelper("gravatarProfileUrl", function() {
    if (Meteor.user()) {
      var email = Meteor.user().emails[0].address;
      return "http://www.gravatar.com/"+Gravatar.hash(email);
    } else {
      return "";
    }
  });


  Template.registerHelper("getUserName", function(userId) {
    console.log("getUserName("+userId+") helper called!");
    var user = Meteor.users.findOne({_id: userId});
    console.log(user);
    if (user && user.profile && user.profile.name) {
      return user.profile.name;
    } else {
      return "Unknown";
    }
  });

  Template.registerHelper("getUserFirstName", function(userId) {
    console.log("getUserFirstName("+userId+") helper called!");
    var user = Meteor.users.findOne({_id: userId});
    console.log(user);
    if (user && user.profile && user.profile.firstName) {
      return user.profile.firstName;
    } else {
      return "Unknown";
    }
  });

  Template.registerHelper("getUserLastName", function(userId) {
    console.log("getUserLastName("+userId+") helper called!");
    var user = Meteor.users.findOne({_id: userId});
    console.log(user);
    if (user && user.profile && user.profile.lastName) {
      return user.profile.lastName;
    } else {
      return "Unknown";
    }
  });

  Template.registerHelper("getUserBiography", function(userId) {
    console.log("getUserBiography("+userId+") helper called!");
    var user = Meteor.users.findOne({_id: userId});
    console.log(user);
    if (user && user.profile) {
      return user.profile.biography;
    } else {
      return "?";
    }
  });


  // Collection access by primary key
  Template.registerHelper("getUser", function(userId) {
    return Meteor.users.findOne({_id: userId});
  });

  Template.registerHelper("getCourse", function(courseId) {
    return Courses.findOne({_id: courseId});
  });

  Template.registerHelper("getTheme", function(themeId) {
    return Themes.findOne({_id: themeId});
  });

  Template.registerHelper("getTopic", function(topicId) {
    return Topics.findOne({_id: topicId});
  });

  Template.registerHelper("getSkill", function(skillId) {
    return Skills.findOne({_id: skillId});
  });

  Template.registerHelper("getLab", function(labId) {
    return Labs.findOne({_id: labId});
  });

  Template.registerHelper("getContentItem", function(itemId) {
    return ContentItems.findOne({_id: itemId});
  });


  // Collection access not via primary key
  Template.registerHelper("getSkillsByTopic", function(topicId) {
    return Skills.find({topicId: topicId});
  });
  Template.registerHelper("getStudyTasksByTopic", function(topicId) {
    return StudyTasks.find({topicId: topicId});
  });
  Template.registerHelper("getTopicChecksByMasteryCheck", function(masteryCheckId) {
    return TopicChecks.find({masteryCheckId: masteryCheckId});
  });

  Template.registerHelper("findStudentRegistrationByUser", function(studentId) {
    return StudentRegistrations.findOne({studentId: studentId});
  });
  Template.registerHelper("findStudentRegistrationByCourseAndUser", function(courseId, studentId) {
    return StudentRegistrations.findOne({courseId: courseId, studentId: studentId});
  });


  // other stuff
  Template.registerHelper("course", function() {
    return Session.get("course");
  });

  Template.registerHelper("courseId", function() {
    var course = Session.get("course");
    return course?course._id:undefined;
  });


  // counts
  Template.registerHelper("getStudentCountForCourse", function(courseId) {
    return StudentRegistrations.find({courseId: courseId}).count();
  });

  Template.registerHelper("getThemeCountForCourse", function(courseId) {
    return Themes.find({courseId: courseId, removed: {$ne: true}}).count();
  });

  Template.registerHelper("getTopicCountForCourse", function(courseId) {
    return Topics.find({courseId: courseId, removed: {$ne: true}}).count();
  });

  Template.registerHelper("getSkillCountForCourse", function(courseId) {
    return Skills.find({courseId: courseId, removed: {$ne: true}}).count();
  });

  Template.registerHelper("getStudyTaskCountForCourse", function(courseId) {
    return StudyTasks.find({courseId: courseId, removed: {$ne: true}}).count();
  });

  Template.registerHelper("getTopicCountForTheme", function(themeId) {
    return Topics.find({themeId: themeId, removed: {$ne: true}}).count();
  });


  // registration helper methods (self)
  Template.registerHelper("selfRegisterAsStudent", function(courseId) {
    Meteor.call('selfRegisterAsStudent', courseId, function (error, result) {
      if (error) {
        alert("registration failed: "+error.error);
      }
    });
  });

  Template.registerHelper("isMe", function(userId) {
    console.log("isMe("+userId+")");
    return Meteor.userId()===userId;
  });

  Template.registerHelper("isAdmin", function() {
    return isAdmin();
  });

  Template.registerHelper("isRegisteredAsAnything", function(courseId) {
    return (!!StudentRegistrations.findOne({studentId: Meteor.userId(), courseId: courseId}))
      || (!!AssistantRegistrations.findOne({assistantId: Meteor.userId(), courseId: courseId}))
      || (!!InstructorRegistrations.findOne({instructorId: Meteor.userId(), courseId: courseId}));
  });

  Template.registerHelper("isRegisteredAsStudent", function(courseId) {
    //console.log("isRegisteredAsStudent("+courseId+")");
    return !!StudentRegistrations.findOne({studentId: Meteor.userId(), courseId: courseId});
  });

  Template.registerHelper("isRegisteredAsAssistant", function(courseId) {
    //console.log("isRegisteredAsAssistant("+courseId+")");
    return !!AssistantRegistrations.findOne({assistantId: Meteor.userId(), courseId: courseId});
  });

  Template.registerHelper("isRegisteredAsInstructor", function(courseId) {
    //console.log("isRegisteredAsInstructor("+courseId+")");
    return !!InstructorRegistrations.findOne({instructorId: Meteor.userId(), courseId: courseId});
  });

  Template.registerHelper("isOnTeachingTeam", function(courseId) {
    return isOnTeachingTeam(courseId);
  });

  Template.registerHelper("isMeOrOnTeachingTeam", function(userId, courseId) {
    //console.log("isMeOrOnTeachingTeam("+courseId+")");
    // TODO: Did this last change break student pages?
    //       (e.g., when going to the "Bugs" or "Mastered" tabs)
    //var result = (Meteor.userId()===userId)
    // NOTE: This really tests whether userId CONTAINS the current user.
    //       Thus, it can be used with a scalar userId,
    //       but also with an array of user ids.
    var result = _.contains(userId, Meteor.userId())
      || (!!AssistantRegistrations.findOne({assistantId: Meteor.userId(), courseId: courseId}))
      || (!!InstructorRegistrations.findOne({instructorId: Meteor.userId(), courseId: courseId}));
    //console.log("isMeOrOnTeachingTeam() -> "+result);
    return result;
  });

  Template.registerHelper("isNotYetApprovedAsStudent", function(courseId) {
    //console.log("isNotYetApprovedAsStudent("+courseId+")");
    return !StudentRegistrations.findOne({studentId: Meteor.userId(), courseId: courseId, approved: true});
  });


  // registration helper methods (to be used by teaching team, i.e., if isOnTeachingTeam(courseId)===true)
  Template.registerHelper("isUserRegisteredAsStudent", function(userId, courseId) {
    //console.log("isUserRegisteredAsStudent("+userId+", "+courseId+")");
    return !!StudentRegistrations.findOne({studentId: userId, courseId: courseId});
  });

  Template.registerHelper("isUserRegisteredAsAssistant", function(userId, courseId) {
    //console.log("isUserRegisteredAsAssistant("+userId+", "+courseId+")");
    return !!AssistantRegistrations.findOne({assistantId: userId, courseId: courseId});
  });

  Template.registerHelper("isUserRegisteredAsInstructor", function(userId, courseId) {
    //console.log("isUserRegisteredAsInstructor("+userId+", "+courseId+")");
    return !!InstructorRegistrations.findOne({instructorId: userId, courseId: courseId});
  });

  Template.registerHelper("isUserNotYetApprovedAsStudent", function(userId, courseId) {
    //console.log("isUserNotYetApprovedAsStudent("+userId+", "+courseId+")");
    return !StudentRegistrations.findOne({studentId: userId, courseId: courseId, approved: true});
  });

  Template.registerHelper("isUserApprovedNotWithdrawnStudent", function(userId, courseId) {
    //console.log("isUserApprovedNotWithdrawnStudent("+userId+", "+courseId+")");
    return isUserApprovedNotWithdrawnStudent(userId, courseId);
  });


  // TODO: is "hasNotYetStartedTopic" deprecated?
  Template.registerHelper("hasNotYetStartedTopic", function(topicId) {
    var topicStatus = TopicStatus.findOne({studentId: Meteor.userId(), topicId: topicId});
    return !topicStatus;
  });

}
