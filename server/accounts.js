// accounts-password configuration
Meteor.startup(function() {
  console.log("Configuring Accounts.emailTemplates");
  Accounts.emailTemplates.from = 'Informa <informa.inf@usi.ch>';
});



// server-only methods (they wouldn't run on the client, because they call code that only exists on the server)

Meteor.methods({
	resendVerificationEmail: function(email) {
		console.log("Meteor.methods.resendVerificationEmail("+email+")");
		//console.log("Accounts:");
		//console.log(Accounts);
		//console.log("Accounts.sendVerificationEmail:");
		//console.log(Accounts.sendVerificationEmail);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to request a resend of the verification mail.");
		}
		Accounts.sendVerificationEmail(this.userId, email);
	},
});
